<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Services\CategorieService;
use App\Http\Services\CommandeService;
use App\Http\Services\ProduitService;
use App\Models\Produit;
use Illuminate\Http\Request;
use Jackiedo\Cart\Facades\Cart;


class FrontendController extends Controller
{
    //

    protected $produitService;
    protected $categorieService;
    protected $commandeService;

    public function __construct(
        ProduitService $_produitService,
        CategorieService $_categorieService,
        CommandeService $_commandeService
    ) {
        $this->produitService  = $_produitService;
        $this->categorieService  = $_categorieService;
        $this->commandeService  = $_commandeService;
    }

    //le portail pour sélectionner les deux sections du site
    public function portal()
    {
        return view('front.portal');
    }


    //l'accueil de la partie architecture
    public function index()
    {
        return view('front.index', [
            "active" => [1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        ]);
    }
    //l'accueil de la partie fourniture
    public function fourniture()
    {
       /*  return view('front.fourniture-index', [
            "active" => [1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        ]); */

        $produits = $this->produitService->getAll();
        $categories = $this->categorieService->getAll();
        return view('front.fourniture-index', [
            "produits" => $produits,
            "categories" => $categories,
            "active" => [1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        ]);
    }

    // la vue apropos
    public function apropos()
    {
        return view('front.apropos', [
            "active" => [0, 1, 0, 0, 0, 0, 0, 0, 0, 0]
        ]);
    }


    // la vue nos-projets
    public function nosprojets()
    {
        return view('front.nosprojets', [
            "active" => [0, 0, 0, 1, 0, 0, 0, 0, 0, 0]
        ]);
    }
    // la vue nos-réalisations
    public function nosrealisations()
    {
        return view('front.realisations', [
            "active" => [0, 0, 0, 1, 0, 0, 0, 0, 0, 0]
        ]);
    }


    // la vue nos-services
    public function services()
    {
        return view('front.services', [
            "active" => [0, 0, 1, 0, 0, 0, 0, 0, 0, 0]
        ]);
    }


    // la vue contacts
    public function contacts()
    {
        return view('front.contacts', [
            "active" => [0, 0, 0, 0, 0, 1, 0, 0, 0, 0]
        ]);
    }


    // la vue store
    public function store()
    {
        $produits = $this->produitService->getAll();
        $categories = $this->categorieService->getAll();
        return view('front.store', [
            "produits" => $produits,
            "categories" => $categories,
            "active" => [0, 0, 0, 0, 1, 0, 0, 0, 0, 0]
        ]);
    }

    /**
     * Details d'un produit
     */
    public function detailsProduit($slug)
    {

        $best_products = $this->produitService->getAll();
        $produit = $this->produitService->getOneBySlug($slug);
        return view('front.details-produit', [
            "best_products" => $best_products,
            "produit" => $produit,
            //"categories" => $categories,
            "active" => [0, 0, 0, 0, 1, 0, 0, 0, 0, 0]
        ]);
    }

    /**
     * Methode pour ajouter au panier
     */
    public function ajoutCommande($idProduit, $qty = 1)
    {
        $produit = $this->produitService->getOne($idProduit);
        $this->commandeService->ajoutPanier($idProduit);
        return redirect()->back()->with(['data' => $produit->intitule]);

        //        return redirect()->back();
    }

    /**
     * ajouter en precisant la quantite
     */
    public function ajoutCommandeQty($idP, Request $request)
    {
        $qty = $request->qty;
        $produit = $this->produitService->getOne($idP);
        $this->commandeService->ajoutPanier($idP, $qty);
        return redirect()->back()->with(['data' => $produit->intitule]);
    }

    /**
     * Affiche le panier
     */
    public function showPanier()
    {

        $best_products = $this->produitService->getAll();
        //$produit = $this->produitService->getOneBySlug($slug);
        return view('front.panier', [
            "best_products" => $best_products,
            //"categories" => $categories,
            "active" => [0, 0, 0, 0, 0, 0, 1, 0, 0, 0]
        ]);
    }

    /**
     * Pour vider le panier
     */
    public function deletePanier(Request $request)
    {
        $this->commandeService->destroyPanier($request);
        return redirect()->back();
    }

    /**
     * Affiche la page de paiement
     */
    public function paiement()
    {
        return view('front.paiement', [
            //"best_products" => $best_products,
            //"categories" => $categories,
            "active" => [0, 0, 0, 0, 0, 0, 0, 1, 0, 0]
        ]);
    }

    public function loginUser(Request $request)
    {
        return view('auth.login-user-paiement');
    }
}
