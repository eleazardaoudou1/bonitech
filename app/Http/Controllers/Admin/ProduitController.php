<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Services\CategorieService;
use App\Http\Services\ProduitService;
use Illuminate\Http\Request;

class ProduitController extends Controller
{
    protected $produitService;
    protected $categorieService;

    public function __construct(ProduitService $_produitService,CategorieService $_categorieService)
    {
        $this->produitService  = $_produitService;
        $this->categorieService  = $_categorieService;
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $produits = $this->produitService->getAll();
        $categories = $this->categorieService->getAll();
        $title_page = "Categories";
        return view('admin.produits.index',[
            "title_page " => $title_page,
            "produits" => $produits,
            "categories" => $categories,
            "active" => [1,0,0,0,0,0,0],
            "fil_ariane" => ["Produits", "Liste"]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        $this->produitService->create($request);
        return redirect()->route('admin.produits.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        // 
        $this->produitService->delete($id);
        return redirect()->route('admin.produits.index');
    }
}
