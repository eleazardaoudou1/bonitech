<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Services\UserService;
use App\Http\Services\RoleService;
use Illuminate\Http\Request;
use App\Models\Role;


class UserController extends Controller
{
    //
    protected $userService;
    protected $roleService;

    public function __construct(UserService $_userService, RoleService $_roleService)
    {
        $this->userService  = $_userService;
        $this->roleService  = $_roleService;
    }

    /**
     * Vue qui retourne les clients
     */
    public function getClients()
    {
    }

    /**
     * Vue qui retourne les admins
     */
    public function getAdmins()
    {
    }
    /**
     * url qui cree 1 admins
     */
    public function storeAdmin()
    {
        $this->roleService->createAdminRoles();
        $this->userService->createAdmin();
    }
    /**
     * Vue qui supprime les users
     */
    public function destroy()
    {
    }

    //reccupérer les roles

    public function getRoles(){
       return Role::all();
    }
}
