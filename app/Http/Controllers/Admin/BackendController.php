<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BackendController extends Controller
{
    //

    //dashboard
    public function index()
    {

        if (auth()->user()->role_id == 1)
            return view('admin.dashboard', [
                "active" => [1, 0, 0, 0, 0, 0, 0]
            ]);
        else
            return view('front.dashboard-client', [
                "active" => [1, 0, 0, 0, 0, 0, 0, 0]
            ]);
    }


    //categeories-produits
    public function categories()
    {
        $categories = [];
        $title_page = "Categories";
        return view('admin.categories', [
            "title_page " => $title_page,
            "categories" => $categories,
            "active" => [1, 0, 0, 0, 0, 0, 0],
            "fil_ariane" => ["Categories", "Liste"]
        ]);
    }


    //post ajouter une nouvelle categorie
    public function ajoutCategories()
    {
    }

    /**
     * redirect to the appropriate dashboard
     */
    public function dashboard()
    {
        if (auth()->user()->role_id == 1)
            return view('admin.dashboard');
        else
            return view('front.dashboard-client');
    }
}
