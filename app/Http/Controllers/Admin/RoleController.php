<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Services\RoleService;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    //

    protected $roleService;

    public function __construct(RoleService $_roleService,)
    {
        $this->roleService  = $_roleService;
    }

    /**
     * To get all roles
     */
    public function index()
    {
        $roles = $this->roleService->getAll();
        //dd($roles);
        $title_page = "Roles";
        return view('admin.roles',[
            "title_page " => $title_page,
            "roles" => $roles,
            "active" => [1,0,0,0,0,0,0],
            "fil_ariane" => ["Categories", "Liste"]
        ]);
    }

    /**
     * To add a new role
     */
    public function store(Request $request)
    {
        $this->roleService->create($request);
        return redirect()->route('admin.roles.index');
    }

    /**
     * To delete a role
     */
    public function destroy()
    {
    }
}
