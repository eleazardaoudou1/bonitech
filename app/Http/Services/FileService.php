<?php

namespace App\Http\Services;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;



class FileService
{


    public static function upload_product_images($product_id, $existings = null)
    {
        if (!request()->hasFile('images')) {
            return false;
        }

        foreach (request()->file('images') as $key => $image) {
            Storage::disk('public')->put('images/', $image);

            self::save_image($product_id, $key, $image);
        }
    }

    public static function save_image($product_id, $key, $image)
    {
        /* $img = Image::where(['product_id' => $product_id])->skip($key)->take(1)->first();

		if ($img) {
			$filename = str_replace('/storage/', '', $img->url);

			Storage::disk('public')->delete($filename);

			$img->url =  $image->hashName();
			$img->save();
		} else {
			Image::create([
				'url' =>  $image->hashName(),
				'product_id' => $product_id,
			]);
		} */
    }

    /**
     * Get all users
     */
    public function getAll()
    {
    }

    /**
     * Get one user
     */
    public function getOne($id)
    {
        # code...
    }

    /**
     * Creer une user
     */
    public function create($request)
    {
    }

    /**
     * Function to upload categorie Image
     */
    public function uploadCategorieProduitImage(Request $request)
    {
        /*    $request->validate($request, [
            'image' => 'required|max:2048',
        ]); */
        $image = $request->file('image');
        $name = $image->hashName();
        Storage::disk('public')->put('images/categories/', $image);
        $destinationPath = public_path('/admin/img/products/categories');
        $image->move($destinationPath, $name);
        return $name;
    }


    /**
     * Function to upload categorie Image
     */
    public function uploadProduitImage(Request $request, $image)
    {
        //$image = $request->file('image');
        $name = $image->hashName();
        Storage::disk('public')->put('images/products', $image);
        $destinationPath = public_path('/admin/img/products');
        $image->move($destinationPath, $name);
        return $name;
    }


    public function fileUpload(Request $request)
    {
        $randomString = Str::random(10);
        $image = $request->file('photo');
        $name = time() . $randomString . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/admin/img/products');
        $image->move($destinationPath, $name);
        return $name;
    }

    /**
     * Mise a jour d'une user
     */
    public function update($request, $id)
    {
    }

    /**
     * Delete a user
     */

    public function delete($id)
    {
    }
}
