<?php

namespace App\Http\Services;

use App\Constants;
use App\Models\Role;

class RoleService
{

    protected $fileService;
    protected $typeProduitService;
    protected $utilsService;

    public function __construct(
        //FileService $_fileService,
        //TypeProduitService $_typeProduitService,
        UtilsService $_utilsService
    ) {
        //$this->fileService  = $_fileService;
        //$this->typeProduitService  = $_typeProduitService;
        $this->utilsService  = $_utilsService;
    }


    /**
     * Get all roles
     */
    public function getAll()
    {
        return Role::all();
    }

    /**
     * Get one role
     */
    public function getOne($id)
    {
        # code...
        return Role::findOrFail($id);
    }

    /**
     * Creer une role
     */
    public function create($request)
    {
        $role = new Role();
        $role->intitule = $request->intitule;
        $role->save();
    }


    public function createAdminRoles(){
        Role::create([
            'id'          => 1,
            'intitule'     => 'superadmin',
        ]);
        Role::create([
            'id'          => 2,
            'intitule'     => 'admin',
        ]);
    }


    /**
     * to delete a role
     */
    public function delete($id)
    {
        $produit = $this->getOne($id);
        $produit->delete();
    }
}
