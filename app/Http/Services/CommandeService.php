<?php

namespace App\Http\Services;

use App\Constants;
use App\Models\Categorie;
use GuzzleHttp\Psr7\Request;
use Jackiedo\Cart\Facades\Cart;
use PDO;

//use Darryldecode\Cart\Cart as CartCart;

class CommandeService
{
    protected $produitService;
    protected $categorieService;

    public function __construct(ProduitService $_produitService, CategorieService $_categorieService)
    {
        $this->produitService  = $_produitService;
        $this->categorieService  = $_categorieService;
    }
    /**
     * Ajout au panier
     */
    public function ajoutPanier($idProduit, $qty=1)
    {
        //
        $produit = $this->produitService->getOne($idProduit);
        $commande = Cart::name('commande');

        $productItem = $commande->addItem(
            [
                'id' => $produit->id,
                'title' => $produit->intitule,
                'quantity' => $qty,
                'price' => $produit->prix,
                /*  'options' => [ 
                'size' => [ 'label' => 'XL', 'value' => 'XL' ], 
                'color' => [ 'label' => 'Red', 'value' => '#f00' ] 
            ], */
                'extra_info' => [
                    'date_time' => ['added_at' => time(),],
                    'details' => [ 'slug' => $produit->slug, 'image' => $produit->image],
                    'slug' => $produit->slug
                ]
            ]
        );

        session([
            'commande' => $commande,
            //'prix_total' => $commande->prix_total
        ]);

        //$recentArticle = $commande->addItem([ 'id' => 2, 'title' => 'Demo artile on the blog', ]);



        //$commande->getExtraInfo('livraison')
        //$commande->removeExtraInfo('livraison.adresse')
        //$commande->getSubtotal() // without taxes
        //$commande->getTotal() //after taxes

        //get the details of the cart details and also be able to covert to JSON

        //$commande->getDetails()->toJson();
    }


    public function destroyPanier( $request){
        $commande = Cart::name('commande');
        $commande->destroy();
        $request->session()->forget('commande');
        $request->session()->flush();
    }
}
