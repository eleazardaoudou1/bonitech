<?php

namespace App\Http\Services;

use App\Constants;
use App\Models\Categorie;

class CategorieService
{

    protected $fileService;
    protected $typeProduitService;
    protected $utilsService;

    public function __construct(
        //FileService $_fileService,
        //TypeProduitService $_typeProduitService,
        UtilsService $_utilsService
    ) {
        //$this->fileService  = $_fileService;
        //$this->typeProduitService  = $_typeProduitService;
        $this->utilsService  = $_utilsService;
    }


     /**
     * Get all categorieProduits
     */
    public function getAll()
    {
        return Categorie::all();
    }

    /**
     * Get one categorieProduit
     */
    public function getOne($id)
    {
        # code...
        return Categorie::findOrFail($id);
    }

     /**
     * Creer une categorieProduit
     */
    public function create($request)
    {
        $categorieProduit = new Categorie();
        $categorieProduit->intitule = $request->intitule;
        $categorieProduit->description = $request->description;
        $categorieProduit->slug = $this->utilsService->createSlug($request->intitule, Constants::CATEGORIE_PRODUIT);
        //$categorieProduit->image = $this->fileService->fileUpload($request);
        $categorieProduit->save();
    }

    //pour supprimer une categorie
    public function delete($id){
        $categorie = $this->getOne($id);
        $categorie->delete();
    }
}