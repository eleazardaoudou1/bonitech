<?php

namespace App\Http\Services;

use App\Constants;
use App\Models\Categorie;
use App\Models\Produit;
//use App\Services\FileService;

class ProduitService
{

    protected $fileService;
    protected $typeProduitService;
    protected $utilsService;

    public function __construct(
        FileService $_fileService,
        //TypeProduitService $_typeProduitService,
        UtilsService $_utilsService
    ) {
        $this->fileService  = $_fileService;
        //$this->typeProduitService  = $_typeProduitService;
        $this->utilsService  = $_utilsService;
    }


     /**
   
     * Get all produits
     */
    public function getAll()
    {
        return Produit::all();
    }

    /**
     * Get recent products
     */
    public function getRecent()
    {
        return Produit::orderBy('created_at', 'DESC')->paginate(4);
    }

    /**
     * Recuperer les produits groupés par catégorie
     */
    public function getAllByCategorie()
    {
        return Produit::all()->groupBy('categorie_produit_id');
    }

     /**
     * Get one produit
     */
    public function getOne($id)
    {
        # code...
        return Produit::findOrFail($id);
    }

    /**
     * Get one produit
     */
    public function getOneBySlug($slug)
    {
        # code...
        return Produit::where('slug', $slug)->first();
    }   

     /**
     * Creer un produit
     */
    public function create($request)
    {
        $produit = new Produit();
        $produit->categorie_id = $request->categorie_id;
        $produit->intitule = $request->intitule;
        $produit->description = $request->description;
        //$produit->details = $request->details;
        $produit->prix = $request->prix;
        //$produit->tags = $request->tags;
        $produit->slug =  $this->utilsService->createSlug($request->intitule, Constants::PRODUIT);
       
        //upload image
        $image1 = $request->file('image');
        //dd($image1);
        $produit->image = $this->fileService->uploadProduitImage($request, $image1);

        $produit->save();
        return $produit;
    }

     /**
     * Delete a produit
     */

     public function delete($id)
     {
         $produit = $this->getOne($id);
         $produit->delete();
     }
}