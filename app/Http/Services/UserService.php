<?php

namespace App\Http\Services;

use App\Constants;
use App\Models\User;
use Illuminate\Support\Facades\Hash;


class UserService
{

    protected $fileService;
    protected $typeProduitService;
    protected $utilsService;

    public function __construct(
        //FileService $_fileService,
        //TypeProduitService $_typeProduitService,
        UtilsService $_utilsService
    ) {
        //$this->fileService  = $_fileService;
        //$this->typeProduitService  = $_typeProduitService;
        $this->utilsService  = $_utilsService;
    }


     /**
     * Get all categorieProduits
     */
    public function getAll()
    {
        return Categorie::all();
    }

    /**
     * Get one categorieProduit
     */
    public function getOne($id)
    {
        # code...
        return Categorie::findOrFail($id);
    }

     /**
     * Creer une categorieProduit
     */
    public function createAdmin()
    {
        User::create([
            'id'          => 5,
            'username'     => 'adeyemi',
            'email' => 'adeyemi@gmail.com',
            'password' => Hash::make('password'),
            'role_id' => 1,
        ]);
    }
}