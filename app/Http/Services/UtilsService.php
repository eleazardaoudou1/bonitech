<?php

namespace App\Http\Services;

use App\Constants;
use App\Models\Categorie;
use App\Models\CategorieProduit;
use App\Models\Parcelle;
use App\Models\Produit;
use Illuminate\Support\Str;


class UtilsService
{

    // SLUG HELPER ==============================================================================
    function createSlug($title, $entity, $id = 0)
    {
        // Normalize the title
        $slug = Str::slug($title);
        // Get any that could possibly be related.
        // This cuts the queries down by doing it once.
        $allSlugs = $this->getRelatedSlugs($slug, $entity, $id);
        // If we haven't used it before then we are all good.
        if (!$allSlugs->contains('slug', $slug)) {
            return $slug;
        }
        // Just append numbers like a savage until we find not used.
        for ($i = 1; $i <= 10; $i++) {
            $newSlug = $slug . '-' . $i;
            if (!$allSlugs->contains('slug', $newSlug)) {
                return $newSlug;
            }
        }
        throw new \Exception('Can not create a unique slug');
    }

    /**
     * Get the related slug of $slug parameter passed
     */
    function getRelatedSlugs($slug, $entity, $id = 0)
    {
        if ($entity == Constants::CATEGORIE_PRODUIT) {
            return Categorie::select('slug')->where('slug', 'like', $slug . '%')
                ->where('id', '<>', $id)
                ->get();
        }
        if ($entity == Constants::PRODUIT) {
            return Produit::select('slug')->where('slug', 'like', $slug . '%')
                ->where('id', '<>', $id)
                ->get();
        }
    }
}
