<?php

namespace App;


class Constants 
{
    /**
     * The application global constants
     */
    public const IMMOBILIER = 1 ; 
    public const RESTAURANT = 2;
    public const AGROBUSINESS = 3;

    //USER OR ADMIN FOR MESSAGES
    public const SENDER_ADMIN = 'ADMIN' ; 
    public const SENDER_USER = 'USER' ; 

    //MODE D'ACHAT
    public const AU_MONTANT = 'MONTANT';

    //ENTITES
    public const PRODUIT = 'PRODUIT';
    public const CATEGORIE_PRODUIT = 'CATEGORIE_PRODUIT';
  
}
