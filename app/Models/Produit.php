<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produit extends Model
{
    use HasFactory;

      //le produit appartient a une categorie
      public function categorie()
      {
          return $this->belongsTo('App\Models\Categorie', 'categorie_id');
      }
}
