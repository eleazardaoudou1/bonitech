<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('produits', function (Blueprint $table) {
            $table->id();
            $table->string('intitule');
            $table->string('slug');
            $table->text('description');
            $table->decimal('prix', 15, 2);
            $table->string('image');
            $table->unsignedBigInteger('categorie_id')->nullable();
            $table->timestamps();

          $table->foreign('categorie_id')
                ->references('id')
                ->on('categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('produits');
    }
};
