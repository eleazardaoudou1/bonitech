<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\User;
use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        Role::create([
            'id'          => 1,
            'intitule'     => 'superadmin',
        ]);
        Role::create([
            'id'          => 2,
            'intitule'     => 'admin',
        ]);


        User::create([
            'id'          => 1,
            'username'     => 'eleazar',
            'email' => 'leprodige70@gmail.com',
            'password' => Hash::make('password'),
            'role_id' => 1,
        ]);
    }
}
