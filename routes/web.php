<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

/*  Route::get('/', function () {
    return view('welcome');
})->name('home');  */

Route::get('/', 'App\Http\Controllers\Front\FrontendController@portal')->name('portal');
Route::get('/architecture-design', 'App\Http\Controllers\Front\FrontendController@index')->name('home');
Route::get('/fourniture', 'App\Http\Controllers\Front\FrontendController@fourniture')->name('fourniture.index');


Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
//frontend
//Route::get('/apropos', 'App\Http\Controllers\Front\FrontendController@apropos')->name('apropos');
Route::get('/apropos', [App\Http\Controllers\Front\FrontendController::class, 'apropos'])->name('apropos');
Route::get('/nos-projets', [App\Http\Controllers\Front\FrontendController::class, 'nosprojets'])->name('nosprojets');
Route::get('/realisations', [App\Http\Controllers\Front\FrontendController::class, 'nosrealisations'])->name('realisations');
Route::get('/contacts', [App\Http\Controllers\Front\FrontendController::class, 'contacts'])->name('contacts');
Route::get('/services', [App\Http\Controllers\Front\FrontendController::class, 'services'])->name('services');
Route::get('/store', [App\Http\Controllers\Front\FrontendController::class, 'store'])->name('store');
Route::get('/details-produit/{slug}', [App\Http\Controllers\Front\FrontendController::class, 'detailsProduit'])->name('single-product');
Route::get('/ajout-commande/produit/{id}/qty/{qty}', [App\Http\Controllers\Front\FrontendController::class, 'ajoutCommande'])->name('ajout-commande');
Route::get('/ajout-commande-qty/produit/{id}', [App\Http\Controllers\Front\FrontendController::class, 'ajoutCommandeQty'])->name('ajout-commande-qty');
Route::get('/panier', [App\Http\Controllers\Front\FrontendController::class, 'showPanier'])->name('panier.show');
Route::get('/vider-panier', [App\Http\Controllers\Front\FrontendController::class, 'deletePanier'])->name('panier.destroy');
Route::get('/login-user', [App\Http\Controllers\Front\FrontendController::class, 'loginUser'])->name('login.user');
Route::get('paiement', [App\Http\Controllers\Front\FrontendController::class, 'paiement'])->name('paiement')->middleware('user.connected');

//debug routes -- A enlever
Route::get('/create-admin', [App\Http\Controllers\Admin\UserController::class, 'storeAdmin'])->name('storeAdmin');
Route::get('/roles', [App\Http\Controllers\Admin\UserController::class, 'getRoles'])->name('getRoles');


/***
 * Les routes de dashboard
 */

//Route::get('/tableau-de-bord', [App\Http\Controllers\Admin\BackendController::class, 'index'])->name('dashboard');


Route::middleware(['auth', 'verified'])->group(function () {
    Route::prefix('admin')->group(function () {



        Route::get('/tableau-de-bord', [App\Http\Controllers\Admin\BackendController::class, 'index'])->name('dashboard');

        /*****************Gestion Categorie************/
        Route::get('/categories-produits', [App\Http\Controllers\Admin\CategorieController::class, 'index'])->name('admin.categories');
        Route::post('/ajout-categories-produits', [App\Http\Controllers\Admin\CategorieController::class, 'store'])->name('admin.categories.store');
        Route::delete('/delete-categories-produits/{id}', [App\Http\Controllers\Admin\CategorieController::class, 'destroy'])->name('admin.categories.delete');
        //Route::get('/categories-produits', [App\Http\Controllers\Admin\BackendController::class, 'categories'])->name('admin.categories');



        /*****************Gestion Produits *********************************************/
        Route::get('/produits', [App\Http\Controllers\Admin\ProduitController::class, 'index'])->name('admin.produits.index');
        Route::post('/ajout-produits', [App\Http\Controllers\Admin\ProduitController::class, 'store'])->name('admin.produits.store');
        Route::delete('/delete-produits/{id}', [App\Http\Controllers\Admin\ProduitController::class, 'destroy'])->name('admin.produits.destroy');

        /*****************Gestion Utilisateurs *********************************************/
        Route::get('/clients', [App\Http\Controllers\Admin\UserController::class, 'getClients'])->name('admin.users.clients');
        Route::get('/admins', [App\Http\Controllers\Admin\UserController::class, 'getAdmins'])->name('admin.users.admins');
        Route::post('/ajout-admin', [App\Http\Controllers\Admin\UserController::class, 'storeAdmin'])->name('admin.users.store');
        Route::delete('/delete-users/{id}', [App\Http\Controllers\Admin\UserController::class, 'destroy'])->name('admin.users.destroy');

        /*****************Gestion Roles *********************************************/
        Route::get('/roles', [App\Http\Controllers\Admin\RoleController::class, 'index'])->name('admin.roles.index');
        Route::post('/ajout-roles', [App\Http\Controllers\Admin\RoleController::class, 'store'])->name('admin.roles.store');
        Route::delete('/delete-roles/{id}', [App\Http\Controllers\Admin\RoleController::class, 'destroy'])->name('admin.roles.destroy');
    });
});
