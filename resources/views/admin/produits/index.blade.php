@extends('layouts.admin.layoutadmin')
@section('content')
    <main id="main" class="main">
        <div class="pagetitle">
            <h1>{{-- $title_page --}}</h1>
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html">{{ $fil_ariane[0] }}</a></li>
                    <li class="breadcrumb-item active">{{ $fil_ariane[1] }}</li>
                </ol>
            </nav>
        </div><!-- End Page Title -->

        <section class="section">
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                Cette page affiche la liste de tous vos produits.
                {{-- <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button> --}}
            </div>
            <div class="row">
                @if (count($produits) == 0)
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Ajouter <span>|
                                        Produit</span></h5>
                                <div class="activity">
                                    {{-- {{ "Aucune dépense planifiée aujourd'hui" }} --}}
                                    <div class="text-center display-4 mt-3">
                                        <a data-bs-toggle="modal" data-bs-target="#basicModalMomo" style="color:inherit">
                                            <i class="bi fa-5x bi-plus-circle"></i>
                                        </a>
                                        <h5 class="card-title">Ajouter un produit</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Ajouter <span>|
                                        Produit</span></h5>
                                <div class="activity">
                                    {{-- {{ "Aucune dépense planifiée aujourd'hui" }} --}}
                                    <div class="text-center display-4 mt-3">
                                        <a data-bs-toggle="modal" data-bs-target="#basicModalMomo" style="color:inherit">
                                            <i class="bi fa-5x bi-plus-circle"></i>
                                        </a>
                                        <h5 class="card-title">Ajouter un produit</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if (count($produits) > 0)
                        <div class="col-lg-12">
                            <div class="card">
                                {{-- {{dd($depensesDatesDistinctes)}} --}}
                                <div class="card-body pt-2"></div>
                                <h4 class="card-header fw-bold d-inline-block" style="color:inherit">Toutes les produits
                                </h4>
                                <div class="card-body">
                                    <?php $i = 1; ?>
                                    <div class="row border-bottom text-center my-auto py-auto">
                                        <div class="col-md-1">*</div>
                                        <div class="col-md-2">Intitule</div>
                                        <div class="col-md-2">Catégorie</div>
                                        <div class="col-md-2">Description</div>
                                        <div class="col-md-1">Prix</div>
                                        <div class="col-md-3">Image</div>
                                        <div class="col-md-1">Actions</div>
                                    </div>
                                    @foreach ($produits as $produit)
                                       
                                        <div class="row mt-3 pb-3 border-bottom text-center">
                                            <div class="col-md-1">{{ $i}}</div>
                                            {{--   <div class="col-md-3">{{ date('d-m-Y', strtotime($categorie->created_at)) }}
                                            </div> --}}
                                            <div class="col-md-2 fw-bold">
                                                {{ $produit->intitule }}

                                            </div>
                                            <div class="col-md-2">{{ $produit->categorie->intitule }}</div>
                                            <div class="col-md-2">{{ $produit->description }}</div>
                                            <div class="col-md-1"> {{ number_format($produit->prix, 0, ',', ' ') }}</div>
                                            <div class="col-md-3">
                                                <img height="100"
                                                    src="{{ asset('admin/img/products/' . $produit->image) }}"
                                                    alt="">
                                            </div>

                                            <div class="col-md-1 ">
                                                <button class="btn btn-danger btn-sm" data-bs-toggle="modal"
                                                    data-bs-target="#deleteDepenseModal{{ $produit->id }}">
                                                    <i class="ri-delete-bin-2-line small"></i>
                                                </button>
                                            </div>

                                            <form method="POST" style="display: inline-block" action="{{route('admin.produits.destroy', $produit->id)}}">
                                                @csrf
                                                @method('DELETE')
                                                <!-- Basic Modal -->
                                                <div class="modal fade" id="deleteDepenseModal{{ $produit->id }}"
                                                    tabindex="-1">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title fw-bold">Supprimer le produit <b
                                                                        class="libelle_depense"></b></h5>
                                                                <button type="button" class="btn-close"
                                                                    data-bs-dismiss="modal" aria-label="Close"></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                Attention!
                                                                Vous supprimez le produit <b
                                                                    class="libelle_depense">{{ $produit->intitule }}</b>
                                                                de la liste de vos produits.
                                                                Cette action
                                                                est irréversible.
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-bs-dismiss="modal">Annuler</button>
                                                                <button type="submit" class="btn btn-danger"
                                                                    id="dep_suppr_btn">Supprimer</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- End Basic Modal-->
                                            </form>
                                        </div>
                                        <?php $i++ ?>
                                    @endforeach

                                    <div class="text-center mt-2">{{-- $produits->links() --}}</div>

                                </div>
                            </div>
                        </div>
                    @endif



                @endif

                <form method="POST" enctype="multipart/form-data" style="display: inline-block"
                    action="{{ route('admin.produits.store') }}" id="ajout_momo_form">
                    @csrf
                    <!-- Basic Modal -->
                    <div class="modal fade" id="basicModalMomo" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Ajouter un produit </h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <div class="modal-body ">

                                    <div class="row mb-3">
                                        <label for="solde" class="col-sm-5 col-form-label fw-bold">Intitule</label>
                                        <div class="col-sm-5">
                                            <input type="text" required name="intitule" class="form-control"
                                                placeholder="intitule">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="libelle" class="col-sm-5 col-form-label fw-bold">Categorie
                                        </label>
                                        <div class="col-sm-5">
                                            <select class="form-select" name="categorie_id"
                                                aria-label="Default select example">
                                                @foreach ($categories as $item)
                                                    <option value="{{ $item->id }}">
                                                        {{ $item->intitule }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="solde" class="col-sm-5 col-form-label fw-bold">slug</label>
                                        <div class="col-sm-5">
                                            <input type="text" required name="slug" class="form-control"
                                                placeholder="slug">
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <label for="solde" class="col-sm-5 col-form-label fw-bold">Prix
                                            Unitaire</label>
                                        <div class="col-sm-5">
                                            <input type="number" required name="prix" min="0"
                                                class="form-control" placeholder="0">
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <label for="details" class="col-sm-5 col-form-label fw-bold">Description</label>
                                        <div class="col-sm-5">
                                            <textarea class="form-control" name="description" style="height: 100px"></textarea>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="details" class="col-sm-5 col-form-label fw-bold">Image</label>
                                        <div class="col-sm-5">
                                            <input type="file" required name="image">
                                        </div>
                                    </div>



                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                        data-bs-dismiss="modal">Annuler</button>
                                    <button type="submit" class="btn btn-success"
                                        id="ajout_compte_momo_btn">Ajouter</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>




        </section>

    </main><!-- End #main -->
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="{{ asset('assets/js/jquery.3.2.1.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/custom/depense-validation-v2.js') }}"></script>

    <script>
        $(document).ready(function() {
            //
            var affiche = false;
            $("#btn_group").click(function(e) {
                if (affiche) {
                    affiche = false;
                    $("#groupedByDay").addClass('d-none');

                } else {
                    affiche = true;
                    $("#groupedByDay").removeClass('d-none');
                    //groupedByDay
                }
            });

            //click sur le boutn supprimer
            $("#dep_suppr_btn").click(function(e) {
                e.preventDefault();
                console.log('ok click');
                $(this).prop('disabled', true);
                var fifthParentElem = $(this).parent().parent().parent().parent().parent();
                console.log(fifthParentElem)
                fifthParentElem.submit();
            });
        });
    </script>
@endsection
