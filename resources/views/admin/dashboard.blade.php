@extends('layouts.admin.layoutadmin')
@section('content')
    <main id="main" class="main">
        <div class="pagetitle">
            <h1>Tableau de bord</h1>
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">Tableau de bord</li>
                </ol>
            </nav>
        </div><!-- End Page Title -->
        <section class="section dashboard">
            <div class="row">
                <!-- Left side columns -->
                <div class="col-lg-8">
                    <div class="row">
                        <!-- Sales Card -->
                        <div class="col-xxl-4 col-md-6">
                            <div class="card info-card sales-card">
                                <div class="card-body">
                                    <h5 class="card-title">Avoir Net <span>|<a style="color: inherit"
                                                href=""> Voir </a> </span></h5>
                                    <div class="d-flex align-items-center">
                                        <div
                                            class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                            <i class="bi bi-currency-exchange"></i>
                                        </div>
                                        <div class="ps-3">
                                            <h6>{{-- number_format($avoir_net, 0, ',', ' ') --}} CFA</h6>
                                            {{-- <span class="text-success small pt-1 fw-bold">12%</span> <span
                                            class="text-muted small pt-2 ps-1">increase</span> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- End Sales Card -->
                        <!-- Revenue Card -->
                        <div class="col-xxl-4 col-md-6">
                            <div class="card info-card revenue-card">

                                <div class="card-body">
                                    <h5 class="card-title">Total Dépenses <span>| Aujourd'hui!</span></h5>

                                    <div class="d-flex align-items-center">
                                        <div
                                            class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                            <i class="bi bi-currency-dollar"></i>
                                        </div>
                                        <div class="ps-3">
                                            <h6>{{-- number_format($depenses_today_total, 0, ',', ' ') --}} CFA</h6>
                                            {{-- <span class="text-success small pt-1 fw-bold">8%</span> <span
                                              class="text-muted small pt-2 ps-1">increase</span> --}}

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div><!-- End Revenue Card -->
                        <!-- Customers Card -->
                        <div class="col-xxl-4 col-xl-12">
                            <div class="card info-card customers-card">
                                <div class="card-body">
                                    <h5 class="card-title">CashFlow <span>| {{-- $month_current --}}
                                            {{-- $year_current --}}</span></h5>
                                    <!-- Vertical Bar Chart -->
                                    <div id="verticalBarChart"
                                        style="min-height: 120px; -webkit-tap-highlight-color: transparent; user-select: none; position: relative;"
                                        class="echart" _echarts_instance_="ec_1649261794849">
                                        <div
                                            style="position: relative; width: 421px; height: 400px; padding: 0px; margin: 0px; border-width: 0px; cursor: default;">
                                            <canvas data-zr-dom-id="zr_0" width="421" height="400"
                                                style="position: absolute; left: 0px; top: 0px; width: 421px; height: 400px; user-select: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); padding: 0px; margin: 0px; border-width: 0px;"></canvas>
                                        </div>
                                        <div class=""
                                            style="position: absolute; display: block; border-style: solid; white-space: nowrap; z-index: 9999999; box-shadow: rgba(0, 0, 0, 0.2) 1px 2px 10px; transition: opacity 0.2s cubic-bezier(0.23, 1, 0.32, 1) 0s, visibility 0.2s cubic-bezier(0.23, 1, 0.32, 1) 0s; background-color: rgb(255, 255, 255); border-width: 1px; border-radius: 4px; color: rgb(102, 102, 102); font: 14px / 21px &quot;Microsoft YaHei&quot;; padding: 10px; top: 0px; left: 0px; transform: translate3d(234px, 87px, 0px); border-color: rgb(255, 255, 255); pointer-events: none; visibility: hidden; opacity: 0;">
                                            <div style="margin: 0px 0 0;line-height:1;">
                                                <div style="margin: 0px 0 0;line-height:1;">
                                                    <div style="font-size:14px;color:#666;font-weight:400;line-height:1;">
                                                        World</div>
                                                    <div style="margin: 10px 0 0;line-height:1;">
                                                        <div style="margin: 0px 0 0;line-height:1;">
                                                            <div style="margin: 0px 0 0;line-height:1;"><span
                                                                    style="display:inline-block;margin-right:4px;border-radius:10px;width:10px;height:10px;background-color:#5470c6;"></span><span
                                                                    style="font-size:14px;color:#666;font-weight:400;margin-left:2px">2011</span><span
                                                                    style="float:right;margin-left:20px;font-size:14px;color:#666;font-weight:900">630,230</span>
                                                                <div style="clear:both"></div>
                                                            </div>
                                                            <div style="clear:both"></div>
                                                        </div>
                                                        <div style="margin: 10px 0 0;line-height:1;">
                                                            <div style="margin: 0px 0 0;line-height:1;"><span
                                                                    style="display:inline-block;margin-right:4px;border-radius:10px;width:10px;height:10px;background-color:#91cc75;"></span><span
                                                                    style="font-size:14px;color:#666;font-weight:400;margin-left:2px">2012</span><span
                                                                    style="float:right;margin-left:20px;font-size:14px;color:#666;font-weight:900">681,807</span>
                                                                <div style="clear:both"></div>
                                                            </div>
                                                            <div style="clear:both"></div>
                                                        </div>
                                                        <div style="clear:both"></div>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                </div>
                                                <div style="clear:both"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <script>
                                        document.addEventListener("DOMContentLoaded", () => {
                                            echarts.init(document.querySelector("#verticalBarChart")).setOption({
                                                title: {
                                                    text: ''
                                                },
                                                tooltip: {
                                                    trigger: 'axis',
                                                    axisPointer: {
                                                        type: 'shadow'
                                                    }
                                                },
                                                legend: {},
                                                grid: {
                                                    left: '3%',
                                                    right: '4%',
                                                    bottom: '3%',
                                                    containLabel: true
                                                },
                                                xAxis: {
                                                    type: 'value',
                                                    boundaryGap: [0, 0.01]
                                                },
                                                yAxis: {
                                                    type: 'category',
                                                    data: ['CashFlow']
                                                },
                                                series: [{
                                                        name: 'Revenus',
                                                        type: 'bar',
                                                        data: []
                                                    },
                                                    {
                                                        name: 'Dépenses',
                                                        type: 'bar',
                                                        data: []
                                                    }
                                                ]
                                            });
                                        });
                                    </script>
                                    <!-- End Vertical Bar Chart -->
                                </div>
                            </div>
                        </div><!-- End Customers Card -->

                        @if (true)
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Répartition des dépenses</h5>

                                        <!-- Pie Chart -->
                                        <div id="pieChart"
                                            style="min-height: 300px; -webkit-tap-highlight-color: transparent; user-select: none; position: relative;"
                                            class="echart" _echarts_instance_="ec_1649265858644">
                                            <div
                                                style="position: relative; width: 421px; height: 500px; padding: 0px; margin: 0px; border-width: 0px; cursor: default;">
                                                <canvas data-zr-dom-id="zr_0" width="421" height="400"
                                                    style="position: absolute; left: 0px; top: 0px; width: 500px; height: 500px; user-select: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); padding: 0px; margin: 0px; border-width: 0px;"></canvas>
                                            </div>
                                            <div class=""></div>
                                        </div>

                                        <script>
                                            document.addEventListener("DOMContentLoaded", () => {
                                                echarts.init(document.querySelector("#pieChart")).setOption({
                                                    title: {
                                                        text: '',
                                                        subtext: 'Répartition des dépenses',
                                                        left: 'center'
                                                    },
                                                    tooltip: {
                                                        trigger: 'item'
                                                    },
                                                    legend: {
                                                        orient: 'vertical',
                                                        left: 'left'
                                                    },
                                                    series: [{
                                                        name: 'Catégories',
                                                        type: 'pie',
                                                        radius: '50%',
                                                        data: 
                                                             [{
                                                                    value: 1048,
                                                                    name: 'Search Engine'
                                                                },
                                                                {
                                                                    value: 735,
                                                                    name: 'Direct'
                                                                },
                                                                {
                                                                    value: 580,
                                                                    name: 'Email'
                                                                },
                                                                {
                                                                    value: 484,
                                                                    name: 'Union Ads'
                                                                },
                                                                {
                                                                    value: 300,
                                                                    name: 'Video Ads'
                                                                }
                                                            ]
                                                            ,
                                                        emphasis: {
                                                            itemStyle: {
                                                                shadowBlur: 10,
                                                                shadowOffsetX: 0,
                                                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                                                            }
                                                        }
                                                    }]
                                                });
                                            });
                                        </script>
                                        <!-- End Pie Chart -->

                                    </div>
                                </div>
                            </div>
                        @endif

                        <!-- Revenue Card -->
                        <div class="col-xxl-4 col-md-6">
                            <div class="card info-card revenue-card">
                                <div class="card-body">
                                    <h5 class="card-title">Tous les comptes <span>|<a style="color: inherit"
                                                href="{{-- route('comptes.index') --}}"> Solde </a> </span></h5>
                                    <div class="d-flex align-items-center">
                                        <div
                                            class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                            <i class="bi bi-currency-exchange"></i>
                                        </div>
                                        <div class="ps-3">
                                            <h6>{{-- number_format($comptes->sum('solde'), 0, ',', ' ')  --}} CFA</h6>
                                            {{-- <span class="text-success small pt-1 fw-bold">8%</span> <span
                                              class="text-muted small pt-2 ps-1">increase</span> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- End Revenue Card -->
                        <!-- Revenue Card -->
                        <div class="col-xxl-4 col-md-6">
                            <div class="card info-card revenue-card">
                                <div class="card-body">
                                    <h5 class="card-title">Budgets en cours <span>|<a style="color: inherit"
                                                href="{{-- route('budgets.index') --}}"> Voir </a> </span></h5>
                                    <div class="d-flex align-items-center">
                                        <div
                                            class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                            <i class="bx bx-bar-chart"></i>
                                        </div>
                                        <div class="ps-3">
                                            <h6>{{-- $nbre_budgets --}}</h6>
                                            {{-- <span class="text-success small pt-1 fw-bold">8%</span> <span
                                              class="text-muted small pt-2 ps-1">increase</span> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- End Revenue Card -->
                        <!-- Revenue Card -->
                        <div class="col-xxl-4 col-md-6">
                            <div class="card info-card customers-card">
                                <div class="card-body">
                                    <h5 class="card-title">Factures impayées <span>|<a style="color: inherit"
                                                href="{{-- route('factures.index') --}}"> Voir </a> </span></h5>
                                    <div class="d-flex align-items-center">
                                        <div
                                            class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                            <i class="ri-bill-line"></i>
                                        </div>
                                        <div class="ps-3">
                                            <h6>{{-- $nbre_factures_impayees->count() --}}</h6>
                                            <span
                                                class="text-danger small pt-1 fw-bold">{{-- number_format($nbre_factures_impayees->sum('montant'), 0, ',', ' ') --}}</span>
                                            <span class="text-muted small pt-2 ps-1"> CFA</span>

                                            {{-- <span class="text-success small pt-1 fw-bold">8%</span> <span
                                              class="text-muted small pt-2 ps-1">increase</span> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- End Revenue Card -->

                        <!-- Reports -->
                        {{-- <div class="col-12">
                         <div class="card">

                              <div class="filter">
                                <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
                                <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                                  <li class="dropdown-header text-start">
                                    <h6>Filter</h6>
                                  </li>

                                  <li><a class="dropdown-item" href="#">Today</a></li>
                                  <li><a class="dropdown-item" href="#">This Month</a></li>
                                  <li><a class="dropdown-item" href="#">This Year</a></li>
                                </ul>
                              </div>

                              <div class="card-body">
                                <h5 class="card-title">Reports <span>/Today</span></h5>

                                <!-- Line Chart -->
                                <div id="reportsChart"></div>

                                <script>
                                  document.addEventListener("DOMContentLoaded", () => {
                                    new ApexCharts(document.querySelector("#reportsChart"), {
                                      series: [{
                                        name: 'Sales',
                                        data: [31, 40, 28, 51, 42, 82, 56],
                                      }, {
                                        name: 'Revenue',
                                        data: [11, 32, 45, 32, 34, 52, 41]
                                      }, {
                                        name: 'Customers',
                                        data: [15, 11, 32, 18, 9, 24, 11]
                                      }],
                                      chart: {
                                        height: 350,
                                        type: 'area',
                                        toolbar: {
                                          show: false
                                        },
                                      },
                                      markers: {
                                        size: 4
                                      },
                                      colors: ['#4154f1', '#2eca6a', '#ff771d'],
                                      fill: {
                                        type: "gradient",
                                        gradient: {
                                          shadeIntensity: 1,
                                          opacityFrom: 0.3,
                                          opacityTo: 0.4,
                                          stops: [0, 90, 100]
                                        }
                                      },
                                      dataLabels: {
                                        enabled: false
                                      },
                                      stroke: {
                                        curve: 'smooth',
                                        width: 2
                                      },
                                      xaxis: {
                                        type: 'datetime',
                                        categories: ["2018-09-19T00:00:00.000Z", "2018-09-19T01:30:00.000Z", "2018-09-19T02:30:00.000Z", "2018-09-19T03:30:00.000Z", "2018-09-19T04:30:00.000Z", "2018-09-19T05:30:00.000Z", "2018-09-19T06:30:00.000Z"]
                                      },
                                      tooltip: {
                                        x: {
                                          format: 'dd/MM/yy HH:mm'
                                        },
                                      }
                                    }).render();
                                  });
                                </script>
                                <!-- End Line Chart -->

                              </div>

                            </div>
                          </div> --}}
                        <!-- End Reports -->

                        <!-- Recent Sales -->
                        {{-- <div class="col-12">
                            <div class="card recent-sales">

                              <div class="filter">
                                <a class="icon" href="#" data-bs-toggle="dropdown"><i class="_bi bi-three-dots"></i></a>
                                <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                                  <li class="dropdown-header text-start">
                                    <h6>Filter</h6>
                                  </li>
                                  <li><a class="dropdown-item" href="#">Today</a></li>
                                  <li><a class="dropdown-item" href="#">This Month</a></li>
                                  <li><a class="dropdown-item" href="#">This Year</a></li>
                                </ul>
                              </div>
                              <div class="card-body">
                                <h5 class="card-title">Dépenses<span>| Récemment ajoutées</span></h5>
                                <table class="table table-borderless datatable">
                                  <thead>
                                    <tr>
                                      <th scope="col">#</th>
                                      <th scope="col">Nom et Prénoms</th>
                                      <th scope="col">Date délivrance</th>
                                      <th scope="col">Profession</th>
                                      <th scope="col">Status</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <th scope="row"><a href="#">#</a></th>
                                      <td>patrice . talon</td>
                                      <td><a href="#" class="text-primary">19/05/1965</a></td>
                                      <td>CEO</td>
                                      <td><span class="badge bg-success">Délivrée</span></td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div> --}}
                        <!-- End Recent Sales -->

                        <!-- Top Selling -->
                        {{-- <div class="col-12">
                            <div class="card top-selling">

                              <div class="filter">
                                <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
                                <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                                  <li class="dropdown-header text-start">
                                    <h6>Filter</h6>
                                  </li>

                                  <li><a class="dropdown-item" href="#">Today</a></li>
                                  <li><a class="dropdown-item" href="#">This Month</a></li>
                                  <li><a class="dropdown-item" href="#">This Year</a></li>
                                </ul>
                              </div>

                              <div class="card-body pb-0">
                                <h5 class="card-title">Top Selling <span>| Today</span></h5>

                                <table class="table table-borderless">
                                  <thead>
                                    <tr>
                                      <th scope="col">Preview</th>
                                      <th scope="col">Product</th>
                                      <th scope="col">Price</th>
                                      <th scope="col">Sold</th>
                                      <th scope="col">Revenue</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <th scope="row"><a href="#"><img src="assets/img/product-1.jpg" alt=""></a></th>
                                      <td><a href="#" class="text-primary fw-bold">Ut inventore ipsa voluptas nulla</a></td>
                                      <td>$64</td>
                                      <td class="fw-bold">124</td>
                                      <td>$5,828</td>
                                    </tr>
                                    <tr>
                                      <th scope="row"><a href="#"><img src="assets/img/product-2.jpg" alt=""></a></th>
                                      <td><a href="#" class="text-primary fw-bold">Exercitationem similique doloremque</a></td>
                                      <td>$46</td>
                                      <td class="fw-bold">98</td>
                                      <td>$4,508</td>
                                    </tr>
                                    <tr>
                                      <th scope="row"><a href="#"><img src="assets/img/product-3.jpg" alt=""></a></th>
                                      <td><a href="#" class="text-primary fw-bold">Doloribus nisi exercitationem</a></td>
                                      <td>$59</td>
                                      <td class="fw-bold">74</td>
                                      <td>$4,366</td>
                                    </tr>
                                    <tr>
                                      <th scope="row"><a href="#"><img src="assets/img/product-4.jpg" alt=""></a></th>
                                      <td><a href="#" class="text-primary fw-bold">Officiis quaerat sint rerum error</a></td>
                                      <td>$32</td>
                                      <td class="fw-bold">63</td>
                                      <td>$2,016</td>
                                    </tr>
                                    <tr>
                                      <th scope="row"><a href="#"><img src="assets/img/product-5.jpg" alt=""></a></th>
                                      <td><a href="#" class="text-primary fw-bold">Sit unde debitis delectus repellendus</a></td>
                                      <td>$79</td>
                                      <td class="fw-bold">41</td>
                                      <td>$3,239</td>
                                    </tr>
                                  </tbody>
                                </table>

                              </div>

                            </div>
                          </div> --}}
                        <!-- End Top Selling -->

                    </div>
                </div><!-- End Left side columns -->

                <!-- Right side columns -->
                <div class="col-lg-4">

                    <!-- Recent Activity -->
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Dépenses<span>|
                                    Ajouter une dépense</span></h5>
                            <div class="activity">
                                {{-- {{ "Aucune dépense planifiée aujourd'hui" }} --}}
                                <div class="text-center display-4 mt-3">
                                    <a href="{{-- route('depenses.index') --}}" style="color:inherit">
                                        <i class="bi fa-5x bi-plus-circle"></i>
                                    </a>
                                    <h5 class="card-title">Ajouter une dépense</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Recent Activity -->

                    <!-- Recent Activity -->
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Budget<span> |
                                    Budget de dépense</span></h5>
                            <div class="activity">
                                {{-- {{ "Aucune dépense planifiée aujourd'hui" }} --}}
                                <div class="text-center display-4 mt-3">
                                    <a href="{{-- route('budgets.index') --}}" style="color:inherit">
                                        <i class="bi fa-5x bi-plus-circle"></i>
                                    </a>
                                    <h5 class="card-title">Ajouter un budget de dépense</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Recent Activity -->

                    <!-- Recent Activity -->
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Les derniers articles <span>| Blog </span></h5>
                            <div class="activity">
                                <div class="text-center display-4_ mt-3">
                                    <ul>
                                        <li class="mb-2"> Comment économiser avec Finanxee et atteindre
                                            l'indépendance financière. <a href="">Très prochainement ...</a></li>
                                        <li class="mb-2">Trois (03) méthodes pour rester dans son budget. <a
                                                href="">Très prochainement ...</a></li>
                                        <li class="mb-2">Sept (07) choses que vous pouvez faire avec Finanxee. <a
                                                href="">Très prochainement ...</a></li>
                                    </ul>
                                    {{-- <h5 class="card-title">Planifiez vos dépenses</h5> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Recent Activity -->
                    <!-- Budget Report -->
                    {{-- <div class="card">
                            <div class="filter">
                              <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
                              <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                                <li class="dropdown-header text-start">
                                  <h6>Filter</h6>
                                </li>

                                <li><a class="dropdown-item" href="#">Today</a></li>
                                <li><a class="dropdown-item" href="#">This Month</a></li>
                                <li><a class="dropdown-item" href="#">This Year</a></li>
                              </ul>
                            </div>

                            <div class="card-body pb-0">
                              <h5 class="card-title">Budget Report <span>| This Month</span></h5>

                              <div id="budgetChart" style="min-height: 400px;" class="echart"></div>

                              <script>
                                document.addEventListener("DOMContentLoaded", () => {
                                  var budgetChart = echarts.init(document.querySelector("#budgetChart")).setOption({
                                    legend: {
                                      data: ['Allocated Budget', 'Actual Spending']
                                    },
                                    radar: {
                                      // shape: 'circle',
                                      indicator: [{
                                        name: 'Sales',
                                        max: 6500
                                      },
                                      {
                                        name: 'Administration',
                                        max: 16000
                                      },
                                      {
                                        name: 'Information Technology',
                                        max: 30000
                                      },
                                      {
                                        name: 'Customer Support',
                                        max: 38000
                                      },
                                      {
                                        name: 'Development',
                                        max: 52000
                                      },
                                      {
                                        name: 'Marketing',
                                        max: 25000
                                      }
                                      ]
                                    },
                                    series: [{
                                      name: 'Budget vs spending',
                                      type: 'radar',
                                      data: [{
                                        value: [4200, 3000, 20000, 35000, 50000, 18000],
                                        name: 'Allocated Budget'
                                      },
                                      {
                                        value: [5000, 14000, 28000, 26000, 42000, 21000],
                                        name: 'Actual Spending'
                                      }
                                      ]
                                    }]
                                  });
                                });
                              </script>

                            </div>
                            </div> --}}
                    <!-- End Budget Report -->

                    <!-- Website Traffic -->
                    {{-- <div class="card">
                            <div class="filter">
                              <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
                              <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                                <li class="dropdown-header text-start">
                                  <h6>Filter</h6>
                                </li>

                                <li><a class="dropdown-item" href="#">Today</a></li>
                                <li><a class="dropdown-item" href="#">This Month</a></li>
                                <li><a class="dropdown-item" href="#">This Year</a></li>
                              </ul>
                            </div>

                            <div class="card-body pb-0">
                              <h5 class="card-title">Website Traffic <span>| Today</span></h5>

                              <div id="trafficChart" style="min-height: 400px;" class="echart"></div>

                              <script>
                                document.addEventListener("DOMContentLoaded", () => {
                                  echarts.init(document.querySelector("#trafficChart")).setOption({
                                    tooltip: {
                                      trigger: 'item'
                                    },
                                    legend: {
                                      top: '5%',
                                      left: 'center'
                                    },
                                    series: [{
                                      name: 'Access From',
                                      type: 'pie',
                                      radius: ['40%', '70%'],
                                      avoidLabelOverlap: false,
                                      label: {
                                        show: false,
                                        position: 'center'
                                      },
                                      emphasis: {
                                        label: {
                                          show: true,
                                          fontSize: '18',
                                          fontWeight: 'bold'
                                        }
                                      },
                                      labelLine: {
                                        show: false
                                      },
                                      data: [{
                                        value: 1048,
                                        name: 'Search Engine'
                                      },
                                      {
                                        value: 735,
                                        name: 'Direct'
                                      },
                                      {
                                        value: 580,
                                        name: 'Email'
                                      },
                                      {
                                        value: 484,
                                        name: 'Union Ads'
                                      },
                                      {
                                        value: 300,
                                        name: 'Video Ads'
                                      }
                                      ]
                                    }]
                                  });
                                });
                              </script>

                            </div>
                          </div> --}}
                    <!-- End Website Traffic -->

                    {{-- <!-- News & Updates Traffic -->
                          <div class="card">
                            <div class="filter">
                              <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
                              <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                                <li class="dropdown-header text-start">
                                  <h6>Filter</h6>
                                </li>

                                <li><a class="dropdown-item" href="#">Today</a></li>
                                <li><a class="dropdown-item" href="#">This Month</a></li>
                                <li><a class="dropdown-item" href="#">This Year</a></li>
                              </ul>
                            </div>

                            <div class="card-body pb-0">
                              <h5 class="card-title">News &amp; Updates <span>| Today</span></h5>

                              <div class="news">
                                <div class="post-item clearfix">
                                  <img src="assets/img/news-1.jpg" alt="">
                                  <h4><a href="#">Nihil blanditiis at in nihil autem</a></h4>
                                  <p>Sit recusandae non aspernatur laboriosam. Quia enim eligendi sed ut harum...</p>
                                </div>

                                <div class="post-item clearfix">
                                  <img src="assets/img/news-2.jpg" alt="">
                                  <h4><a href="#">Quidem autem et impedit</a></h4>
                                  <p>Illo nemo neque maiores vitae officiis cum eum turos elan dries werona nande...</p>
                                </div>

                                <div class="post-item clearfix">
                                  <img src="assets/img/news-3.jpg" alt="">
                                  <h4><a href="#">Id quia et et ut maxime similique occaecati ut</a></h4>
                                  <p>Fugiat voluptas vero eaque accusantium eos. Consequuntur sed ipsam et totam...</p>
                                </div>

                                <div class="post-item clearfix">
                                  <img src="assets/img/news-4.jpg" alt="">
                                  <h4><a href="#">Laborum corporis quo dara net para</a></h4>
                                  <p>Qui enim quia optio. Eligendi aut asperiores enim repellendusvel rerum cuder...</p>
                                </div>

                                <div class="post-item clearfix">
                                  <img src="assets/img/news-5.jpg" alt="">
                                  <h4><a href="#">Et dolores corrupti quae illo quod dolor</a></h4>
                                  <p>Odit ut eveniet modi reiciendis. Atque cupiditate libero beatae dignissimos eius...</p>
                                </div>

                              </div><!-- End sidebar recent posts-->
                            </div>
                          </div> --}}
                    <!-- End News & Updates -->
                </div><!-- End Right side columns -->
            </div>
        </section>
    </main><!-- End #main -->
@endsection
