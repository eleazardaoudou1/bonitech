@extends('layouts.admin.layoutadmin')
@section('content')
    <main id="main" class="main">
        <div class="pagetitle">
            <h1>{{-- $title_page --}}</h1>
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html">{{ $fil_ariane[0] }}</a></li>
                    <li class="breadcrumb-item active">{{ $fil_ariane[1] }}</li>
                </ol>
            </nav>
        </div><!-- End Page Title -->

        <section class="section">
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                Cette page affiche la liste de tous les roles. 
                {{-- <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button> --}}
            </div>
            <div class="row">
                @if ($roles->count() == 0)
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Ajouter <span>|
                                        Role</span></h5>
                                <div class="activity">
                                    {{-- {{ "Aucune dépense planifiée aujourd'hui" }} --}}
                                    <div class="text-center display-4 mt-3">
                                        <a data-bs-toggle="modal" data-bs-target="#basicModalMomo" style="color:inherit">
                                            <i class="bi fa-5x bi-plus-circle"></i>
                                        </a>
                                        <h5 class="card-title">Ajouter un role</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    @if ($roles->count() > 0)
                        <div class="col-lg-8">
                            <div class="card">
                                {{-- {{dd($depensesDatesDistinctes)}} --}}
                                <div class="card-body pt-2"></div>
                                <h4 class="card-header fw-bold d-inline-block" style="color:inherit">Tous les roles
                                </h4>
                                <div class="card-body">
                                    @foreach ($roles as $categorie)
                                        <div class="row my-auto py-1 border-bottom text-center">
                                            <div class="col-md-3">{{ date('d-m-Y', strtotime($categorie->created_at)) }}
                                            </div>
                                            <div class="col-md-3 fw-bold">
                                                {{ $categorie->intitule }}

                                            </div>
                                            <div class="col-md-3">{{ $categorie->description }}</div>

                                            <div class="col-md-2 ">
                                                <button class="btn btn-danger btn-sm" data-bs-toggle="modal"
                                                    data-bs-target="#deleteDepenseModal{{ $categorie->id }}">
                                                    <i class="ri-delete-bin-2-line small"></i>
                                                </button>

                                            </div>
                                            <form method="POST" style="display: inline-block" action="">
                                                @csrf
                                                @method('DELETE')
                                                <!-- Basic Modal -->
                                                <div class="modal fade" id="deleteDepenseModal{{ $categorie->id }}"
                                                    tabindex="-1">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title fw-bold">Supprimer le role <b
                                                                        class="libelle_depense"></b></h5>
                                                                <button type="button" class="btn-close"
                                                                    data-bs-dismiss="modal" aria-label="Close"></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                Attention!
                                                                Vous supprimez le role <b
                                                                    class="libelle_depense">{{ $categorie->intitule }}</b>
                                                                de la liste de vos roles.
                                                                Cette action
                                                                est irréversible.
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-bs-dismiss="modal">Annuler</button>
                                                                <button type="submit" class="btn btn-danger"
                                                                    id="dep_suppr_btn">Supprimer</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- End Basic Modal-->
                                            </form>
                                        </div>
                                    @endforeach

                                    <div class="text-center mt-2">{{-- $categories->links() --}}</div>

                                </div>
                            </div>
                        </div>
                    @endif


                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Ajouter <span>|
                                        role</span></h5>
                                <div class="activity">
                                    {{-- {{ "Aucune dépense planifiée aujourd'hui" }} --}}
                                    <div class="text-center display-4 mt-3">
                                        <a data-bs-toggle="modal" data-bs-target="#basicModalMomo" style="color:inherit">
                                            <i class="bi fa-5x bi-plus-circle"></i>
                                        </a>
                                        <h5 class="card-title">Ajouter un role</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <form method="POST" style="display: inline-block" action="{{ route('admin.roles.store') }}"
                id="ajout_momo_form">
                @csrf
                <!-- Basic Modal -->
                <div class="modal fade" id="basicModalMomo" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Ajouter un role </h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="modal-body ">

                                <div class="row mb-3">
                                    <label for="solde" class="col-sm-5 col-form-label fw-bold">Intitule</label>
                                    <div class="col-sm-5">
                                        <input type="text" required name="intitule"  class="form-control"
                                            placeholder="intitule">
                                    </div>
                                </div>
                               
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
                                <button type="submit" class="btn btn-success"
                                    id="ajout_compte_momo_btn">Ajouter</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>




        </section>

    </main><!-- End #main -->
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="{{ asset('assets/js/jquery.3.2.1.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/custom/depense-validation-v2.js') }}"></script>

    <script>
        $(document).ready(function() {
            //
            var affiche = false;
            $("#btn_group").click(function(e) {
                if (affiche) {
                    affiche = false;
                    $("#groupedByDay").addClass('d-none');

                } else {
                    affiche = true;
                    $("#groupedByDay").removeClass('d-none');
                    //groupedByDay
                }
            });

            //click sur le boutn supprimer
            $("#dep_suppr_btn").click(function(e) {
                e.preventDefault();
                console.log('ok click');
                $(this).prop('disabled', true);
                var fifthParentElem = $(this).parent().parent().parent().parent().parent();
                console.log(fifthParentElem)
                fifthParentElem.submit();
            });
        });
    </script>
@endsection
