<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Architecture - Boni Tech</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicon -->
    <link href="img/favicon.ico" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;500;600&family=Teko:wght@400;500;600&display=swap"
        rel="stylesheet">

    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link rel="stylesheet" href="{{ asset('front/lib/animate/animate.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('front/lib/owlcarousel/assets/owl.carousel.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('front/lib/tempusdominus/css/tempusdominus-bootstrap-4.min.css') }}"
        type="text/css">

    <!--link href="lib/animate/animate.min.css" rel="stylesheet">
    <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="lib/tempusdominus/css/tempusdominus-bootstrap-4.min.css" rel="stylesheet" /-->

    <!-- Customized Bootstrap Stylesheet -->
    <!--link href="css/bootstrap.min.css" rel="stylesheet"-->
    <link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css') }}" type="text/css">


    <!-- Template Stylesheet -->
    <!--link href="css/style.css" rel="stylesheet"-->
    <link rel="stylesheet" href="{{ asset('front/css/style.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('front/css/nice-select.css') }}" type="text/css">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="{{ asset('front/lib/tempusdominus/js/moment.min.js') }}"></script>
    <script src="{{ asset('front/lib/tempusdominus/js/tempusdominus-bootstrap-4.min.js') }}"></script>
    <!--FEDAPAY -->
    <script src="https://cdn.fedapay.com/checkout.js?v=1.1.7"></script>

</head>

<body>
    <!-- Spinner Start -->
    <div id="spinner"
        class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
        <div class="spinner-border position-relative text-primary" style="width: 6rem; height: 6rem;" role="status">
        </div>
        <img class="position-absolute top-50 start-50 translate-middle" src="{{ asset('front/img/icons/icon-1.png') }}"
            alt="Icon">
    </div>
    <!-- Spinner End -->


    <!-- Topbar Start -->
    <div class="container-fluid bg-dark p-0 wow fadeIn" data-wow-delay="0.1s">
        <div class="row gx-0 d-none d-lg-flex">
            <div class="col-lg-7 px-5 text-start">
                <div class="h-100 d-inline-flex align-items-center py-3 me-3">
                    <a class="text-body px-2" href="tel:+229 674 048 93"><i
                            class="fa fa-phone-alt text-primary me-2"></i>+229 674 048 93</a>
                    <a class="text-body px-2" href="mailto:service@bonitech.com"><i
                            class="fa fa-envelope-open text-primary me-2"></i>service@bonitech.com</a>
                </div>
            </div>
            <div class="col-lg-5 px-5 text-end">
                <div class="h-100 d-inline-flex align-items-center py-3 me-2">
                    <a class="text-body px-2" href="">Termes et conditions</a>
                    <a class="text-body px-2" href="">Politique de confidentialité</a>
                </div>
                <div class="h-100 d-inline-flex align-items-center">
                    <a class="btn btn-sm-square btn-outline-body me-1" href=""><i
                            class="fab fa-facebook-f"></i></a>
                    <a class="btn btn-sm-square btn-outline-body me-1" href=""><i class="fab fa-twitter"></i></a>
                    <a class="btn btn-sm-square btn-outline-body me-1" href=""><i
                            class="fab fa-linkedin-in"></i></a>
                    <a class="btn btn-sm-square btn-outline-body me-0" href=""><i
                            class="fab fa-instagram"></i></a>
                </div>
            </div>
        </div>
    </div>
    <!-- Topbar End -->



    <!-- Navbar Start -->
    <nav class="navbar navbar-expand-lg bg-white navbar-light sticky-top py-lg-0 px-lg-5 wow fadeIn"
        data-wow-delay="0.1s">
        <a href="{{ route('home') }}" class="navbar-brand ms-4 ms-lg-0">
            <h1 class="text-primary m-0"><img height="50" class="me-3"
                    src="{{ asset('front/img/logos/logo-noir.png') }}" alt="Icon">BoniTech</h1>
        </a>
        <button type="button" class="navbar-toggler me-4" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <div class="navbar-nav ms-auto p-4 p-lg-0">

                {{-- <div class="nav-item dropdown">
                    <a href="{{ route('store') }}" class="nav-link dropdown-toggle"
                        data-bs-toggle="dropdown">Store</a>
                    <div class="dropdown-menu border-0 m-0">
                        <a href="{{ route('store') }}" class="dropdown-item">Store</a>
                    </div>
                </div> --}}

                <a href="{{ route('contacts') }}"
                    class="nav-item nav-link {{ $active[5] ? 'active' : '' }}">Contacts</a>
                <a href="{{ route('panier.show') }}" class="nav-item nav-link {{ $active[3] ? 'active' : '' }}">
                    <i class="fa fa-shopping-cart" aria-hidden="true"></i> (
                    @if (null !== session('commande'))
                        {{ session('commande')->getDetails()->items_count }}
                    @else
                        0
                    @endif
                    )
                </a>
            </div>

            @auth
                <form action="{{ route('logout') }}" method="POST">
                    @csrf
                    <button type="submit" class="btn btn-primary py-2 px-2 d-none d-lg-block">Deconnexion</button>
                </form>
                {{-- <a href="{{route('dashboard')}}" class="btn btn-primary py-2 px-4 d-none d-lg-block">{{auth()->user()->username}}</a> --}}


            @endauth

            @guest
                <a href="{{ route('login') }}" class="btn btn-primary py-2 px-4 d-none d-lg-block">Connexion</a>

            @endguest

        </div>
    </nav>
    <!-- Navbar End -->
    @yield('content')
    <!-- Footer Start -->
    <div class="container-fluid bg-dark text-body footer mt-5 pt-5 px-0 wow fadeIn" data-wow-delay="0.1s">
        <div class="container py-5">
            <div class="row g-5">
                <div class="col-lg-3 col-md-6">
                    <h3 class="text-light mb-4">Adresse</h3>
                    <p class="mb-2"><i class="fa fa-map-marker-alt text-primary me-3"></i>ilot : 270c, quartier
                        Aitchedji; Parcelle:e. Maison DAKO DAOUD DJOTODIA</p>
                    <p class="mb-2"><i class="fa fa-phone-alt text-primary me-3"></i>+229 674 048 93/415 480 54</p>
                    <p class="mb-2"><i class="fa fa-envelope text-primary me-3"></i>bonitech3dcorporation@gmail.com
                    </p>
                    <div class="d-flex pt-2">
                        <a class="btn btn-square btn-outline-body me-1" href=""><i
                                class="fab fa-twitter"></i></a>
                        <a class="btn btn-square btn-outline-body me-1" href=""><i
                                class="fab fa-facebook-f"></i></a>
                        <a class="btn btn-square btn-outline-body me-1" href=""><i
                                class="fab fa-youtube"></i></a>
                        <a class="btn btn-square btn-outline-body me-0" href=""><i
                                class="fab fa-linkedin-in"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h3 class="text-light mb-4">Services</h3>
                    <a class="btn btn-link" href="">Architecture</a>
                    <a class="btn btn-link" href="">3D Animation</a>
                    <a class="btn btn-link" href="">Réalisation de plan</a>
                    <a class="btn btn-link" href="">Design Intérieur</a>
                    <a class="btn btn-link" href="">Fourniture</a>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h3 class="text-light mb-4">Liens Rapides</h3>
                    <a class="btn btn-link" href="">A propos</a>
                    <a class="btn btn-link" href="">Contact Us</a>
                    <a class="btn btn-link" href="">Nos Services</a>
                    <a class="btn btn-link" href="">Termes & Condition</a>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h3 class="text-light mb-4">Newsletter</h3>
                    <p>Recevez nos dernières nouvelles. Rejoignez notre Newsletter</p>
                    <div class="position-relative mx-auto" style="max-width: 400px;">
                        <input class="form-control bg-transparent w-100 py-3 ps-4 pe-5" type="text"
                            placeholder="Votre email">
                        <button type="button"
                            class="btn btn-primary py-2 position-absolute top-0 end-0 mt-2 me-2">Rejoindre</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 text-center text-md-start mb-3 mb-md-0">
                        &copy; <a href="{{ route('home') }}"> BoniTech</a>, Tous droits réservés..
                    </div>
                    <div class="col-md-6 text-center text-md-end">
                        <!--/*** This template is free as long as you keep the footer author’s credit link/attribution link/backlink. If you'd like to use the template without the footer author’s credit link/attribution link/backlink, you can purchase the Credit Removal License from "https://htmlcodex.com/credit-removal". Thank you for your support. ***/-->
                        Site réalisé par <a href="https://htmlcodex.com">Goshen Media Communication</a>
                        <br> Distributed By: <a class="border-bottom" href="https://themewagon.com"
                            target="_blank">ThemeWagon</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer End -->



    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>

    <!-- KKIPAY  -->
    <script src="https://cdn.kkiapay.me/k.js" sandox="true"></script>

    <!--FEDAPAY -->
    <script src="https://cdn.fedapay.com/checkout.js?v=1.1.7"></script>

    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>

    <script src="{{ asset('front/lib/wow/wow.min.js') }}"></script>
    <script src="{{ asset('front/lib/easing/easing.min.js') }}"></script>
    <script src="{{ asset('front/lib/waypoints/waypoints.min.js') }}"></script>
    <script src="{{ asset('front/lib/counterup/counterup.min.js') }}"></script>
    <script src="{{ asset('front/lib/owlcarousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('front/lib/tempusdominus/js/moment.min.js') }}"></script>
    <script src="{{ asset('front/lib/tempusdominus/js/moment-timezone.min.js') }}"></script>
    <script src="{{ asset('front/lib/tempusdominus/js/tempusdominus-bootstrap-4.min.js') }}"></script>

    <!--Template Javascript-->
    <!-- script src = "js/main.js" > < /script-->
    <script src="{{ asset('front/js/main.js') }}"></script>

</body>

</html>
