
@extends('layouts.base_layout')
@section('content')
    <main>
        <div class="container">
            <section class="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-4 col-md-6 d-flex flex-column align-items-center justify-content-center">

                            <div class="d-flex justify-content-center py-4">
                                <a href="{{ route('dashboard') }}" class="logo d-flex align-items-center w-auto">
                                    {{-- <img src="{{ asset('img/logo/logo_png.PNG') }}" alt=""> --}}
                                    <img src="{{ asset('img/logo/logo_officiel.png') }}" alt="">
                                </a>
                            </div><!-- End Logo -->

                            <div class="card mb-3">

                                <div class="card-body">

                                    <div class="pt-4 pb-2">
                                        <h5 class="card-title text-center pb-0 fs-4">Créer un compte</h5>
                                        <p class="text-center small">Entrez vos informations pour créer un compte</p>
                                    </div>

                                    <form method="POST" id="register_form" action="{{ route('register') }}" class="row g-3">
                                        @csrf
                                       {{--  <div class="col-12">
                                            <label for="name" class="form-label">Votre Nom</label>
                                            <input type="text" name="name" value="{{ old('name') }}" autocomplete="name"
                                                autofocus class="form-control @error('name') is-invalid @enderror"
                                                id="name" required>
                                            <div class="invalid-feedback">Veuillez saisir votre nom valide!</div>
                                        </div> --}}

                                        <div class="col-12">
                                            <label for="yourEmail" class="form-label">Votre Email</label>
                                            <input type="email" name="email"
                                                class="form-control @error('email') is-invalid @enderror" id="yourEmail"
                                                value="{{ old('email') }}" autocomplete="email" required>
                                            <div class="invalid-feedback">Veuillez saisir une adresse mail valide</div>
                                        </div>

                                         <div class="col-12">
                                            <label for="yourUsername" class="form-label">Choisissez un identifiant</label>
                                            <div class="input-group has-validation">
                                                <span class="input-group-text" id="inputGroupPrepend">@</span>
                                                <input type="text" name="username" class="form-control" id="username"
                                                    required>
                                            </div>
                                            <div class="invalid-feedback">Veuillez saisir votre nom d'utilisateur.</div>
                                        </div> 

                                        <div class="col-12">
                                            <label for="password" class="form-label">Mot de passe</label>
                                            <input type="password" name="password"
                                                class="form-control  @error('password') is-invalid @enderror" id="password"
                                                autocomplete="new-password" required>
                                            <div class="invalid-feedback">Veuillez saisir votre mot de passe!</div>
                                        </div>

                                        <div class="col-12">
                                            <label for="password_confirmation" class="form-label">Confirmez le mot de
                                                passe</label>
                                            <input type="password" name="password_confirmation" required
                                                autocomplete="new-password"
                                                class="form-control @error('password') is-invalid @enderror"
                                                id="password_confirmation" autocomplete="new-password" required>
                                            <div class="invalid-feedback">Veuillez saisir votre mot de passe!</div>
                                        </div>

                                        <div class="col-12">
                                            <div class="form-check">
                                                <input class="form-check-input" name="terms" type="checkbox" value=""
                                                    id="acceptTerms" required>
                                                <label class="form-check-label" for="acceptTerms">J'ai lu et j'adhère aux <a
                                                        href="#">termes et conditions</a></label>
                                                <div class="invalid-feedback">Vous devez accepter avant de continuer.</div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <button class="btn btn-success w-100 register_btn" type="submit">Valider</button>
                                        </div>
                                        <div class="col-12">
                                            <p class="small mb-0">Avez-vous déjà un compte? <a
                                                    href="{{ route('login') }}">Connexion</a></p>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <script src="{{ asset('js/jquery.3.2.1.min.js') }}"></script>
                            {{-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> --}}
                            <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
                            <script src="{{ asset('js/custom/register-validation.js') }}"></script>
                            <script>
                                $(document).ready(function() {
                                    // validate signup form on keyup and submit
                                    /*  $(this).prop('disabled', true);
                                    }); */
                                    //click sur le boutn supprimer
                                    $(".1register_btn").click(function(e) {
                                        e.preventDefault();
                                        console.log('login button clicked');
                                        $(this).prop('disabled', true);
                                        var formElement = $(this).parent().parent();
                                        console.log(formElement)
                                        formElement.submit();
                                    });
                                });
                            </script>
                            <div class="credits">
                                <!-- All the links in the footer should remain intact. -->
                                <!-- You can delete the links only if you purchased the pro version. -->
                                <!-- Licensing information: https://bootstrapmade.com/license/ -->
                                <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/ -->
                                {{-- Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a> --}}
                            </div>

                        </div>
                    </div>
                </div>

            </section>

        </div>

    </main>
@endsection
