@extends('layouts.front.layout1')
@section('content')
<div class="container-fluid page-header py-5 mb-5 wow fadeIn" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeIn;">
    <div class="container py-5">
        <h1 class="display-1 text-white  slideInDown">Nos réalisations</h1>
        <nav aria-label="breadcrumb animated slideInDown">
            <ol class="breadcrumb text-uppercase mb-0">
                <li class="breadcrumb-item"><a class="text-white" href="{{route('home')}}">Accueil</a></li>
                <li class="breadcrumb-item"><a class="text-white" href="#">Pages</a></li>
                <li class="breadcrumb-item text-primary active" aria-current="page">Nos réalisations</li>
            </ol>
        </nav>
    </div>
</div>

  <!-- Project Start -->
  <div class="container-xxl project py-5">
    <div class="container">
        <div class="text-center mx-auto mb-5 wow fadeInUp" data-wow-delay="0.1s" style="max-width: 600px;">
            <h4 class="section-title">Nos Réalisations</h4>
            <h1 class="display-5 mb-4">Explorez nos réalisations. </h1>
        </div>
        <div class="row g-4 wow fadeInUp" data-wow-delay="0.3s">
            <div class="col-lg-4">
                <div class="nav nav-pills d-flex justify-content-between w-100 h-100 me-4">
                    <button class="nav-link w-100 d-flex align-items-center text-start p-4 mb-4 active" data-bs-toggle="pill" data-bs-target="#tab-pane-1" type="button">
                        <h3 class="m-0">01. VILLA OROU</h3>
                    </button>
                    <button class="nav-link w-100 d-flex align-items-center text-start p-4 mb-4" data-bs-toggle="pill" data-bs-target="#tab-pane-2" type="button">
                        <h3 class="m-0">02. VILLA OURA</h3>
                    </button>
                    <button class="nav-link w-100 d-flex align-items-center text-start p-4 mb-4" data-bs-toggle="pill" data-bs-target="#tab-pane-3" type="button">
                        <h3 class="m-0">03. Villa SONOU </h3>
                    </button>
                    <button class="nav-link w-100 d-flex align-items-center text-start p-4 mb-0" data-bs-toggle="pill" data-bs-target="#tab-pane-4" type="button">
                        <h3 class="m-0">04. Autre villa</h3>
                    </button>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="tab-content w-100">
                    <div class="tab-pane fade show active" id="tab-pane-1">
                        <div class="row g-4">
                            <div class="col-md-6" style="min-height: 350px;">
                                <div class="position-relative h-100">
                                    <img class="position-absolute img-fluid w-100 h-100" src={{asset('front/img/projects/rendus/villa-orou/BAHIMAGES_1SunnYday.jpg') }}
                                        style="object-fit: cover;" alt="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="position-relative h-100">
                                    <img class="position-absolute img-fluid w-100 h-100" src={{asset('front/img/projects/rendus/villa-orou/BAhIMAGES_3MorningonVacation.jpg') }}
                                        style="object-fit: cover;" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                   
                    <div class="tab-pane fade show" id="tab-pane-2">
                        <div class="row g-4">
                            <div class="col-md-6" style="min-height: 350px;">
                                <div class="position-relative h-100">
                                    <img class="position-absolute img-fluid w-100 h-100" src={{asset('front/img/projects/rendus/villa-oura/RIG_1-Sunnyday.jpg') }}
                                        style="object-fit: cover;" alt="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="position-relative h-100">
                                    <img class="position-absolute img-fluid w-100 h-100" src={{asset('front/img/projects/rendus/villa-oura/RIG_6Photo.jpg') }}
                                        style="object-fit: cover;" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                   
                    <div class="tab-pane fade show" id="tab-pane-3">
                        <div class="row g-4">
                            <div class="col-md-6" style="min-height: 350px;">
                                <div class="position-relative h-100">
                                    <img class="position-absolute img-fluid w-100 h-100" src={{asset('front/img/projects/rendus/villa-orou/BAHIMAGES_1SunnYday.jpg') }}
                                        style="object-fit: cover;" alt="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="position-relative h-100">
                                    <img class="position-absolute img-fluid w-100 h-100" src={{asset('front/img/projects/rendus/villa-orou/BAhIMAGES_3MorningonVacation.jpg') }}
                                        style="object-fit: cover;" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Project End -->

    
@endsection