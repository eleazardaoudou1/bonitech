@extends('layouts.front.layout1')
@section('content')
    <div class="container-fluid page-header py-5 mb-5 wow fadeIn" data-wow-delay="0.1s"
        style="visibility: visible; animation-delay: 0.1s; animation-name: fadeIn;">
        <div class="container py-5">
            <h1 class="display-1 text-white  slideInDown">Store</h1>
            <nav aria-label="breadcrumb animated slideInDown">
                <ol class="breadcrumb text-uppercase mb-0">
                    <li class="breadcrumb-item"><a class="text-white" href="{{ route('home') }}">Accueil</a></li>
                    <li class="breadcrumb-item"><a class="text-white" href="#">Pages</a></li>
                    <li class="breadcrumb-item text-white" aria-current="page">Store</li>
                    <li class="breadcrumb-item text-primary active" aria-current="page">Details Produit</li>
                </ol>
            </nav>
        </div>
    </div>
    @if (Session::get('data'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>{{(Session::get('data'))}} a été bien ajouté au panier !</strong> <a href="{{route('panier.show')}}">Voir Panier</a>.
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    <div class="product_image_area">
        <div class="container">
            <div class="row s_product_inner">
                <div class="col-lg-6">
                    <div class="s_Product_carousel owl-carousel owl-theme owl-loaded">
                        <div class="owl-stage-outer">
                            <div class="owl-stage"
                                style="transform: translate3d(-1080px, 0px, 0px); transition: all 0s ease 0s; width: 3780px;">
                                <div class="owl-item cloned" style="width: 540px; margin-right: 0px;">
                                    <div class="single-prd-item">
                                        <img class="img-fluid" src="{{ asset('admin/img/products/' . $produit->image) }}"
                                            alt="">
                                    </div>
                                </div>
                                <div class="owl-item cloned" style="width: 540px; margin-right: 0px;">
                                    <div class="single-prd-item">
                                        <img class="img-fluid" src="{{ asset('admin/img/products/' . $produit->image) }}"
                                            alt="">
                                    </div>
                                </div>
                                <div class="owl-item active" style="width: 540px; margin-right: 0px;">
                                    <div class="single-prd-item">
                                        <img class="img-fluid" src="{{ asset('admin/img/products/' . $produit->image) }}"
                                            alt="">
                                    </div>
                                </div>
                                <div class="owl-item" style="width: 540px; margin-right: 0px;">
                                    <div class="single-prd-item">
                                        <img class="img-fluid" src="img/category/s-p1.jpg" alt="">
                                    </div>
                                </div>
                                <div class="owl-item" style="width: 540px; margin-right: 0px;">
                                    <div class="single-prd-item">
                                        <img class="img-fluid" src="img/category/s-p1.jpg" alt="">
                                    </div>
                                </div>
                                <div class="owl-item cloned" style="width: 540px; margin-right: 0px;">
                                    <div class="single-prd-item">
                                        <img class="img-fluid" src="img/category/s-p1.jpg" alt="">
                                    </div>
                                </div>
                                <div class="owl-item cloned" style="width: 540px; margin-right: 0px;">
                                    <div class="single-prd-item">
                                        <img class="img-fluid" src="img/category/s-p1.jpg" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="owl-controls">
                            <div class="owl-nav">
                                <div class="owl-prev" style="display: none;">prev</div>
                                <div class="owl-next" style="display: none;">next</div>
                            </div>
                            <div class="owl-dots" style="">
                                <div class="owl-dot active"><span></span></div>
                                <div class="owl-dot"><span></span></div>
                                <div class="owl-dot"><span></span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 offset-lg-1">
                    <form action="{{ route('ajout-commande-qty', $produit->id) }}" method="GET">
                        @csrf
                        <div class="s_product_text">
                            <h3>{{ $produit->intitule }}</h3>
                            <h2>{{ $produit->prix }} CFA</h2>
                            <ul class="list">
                                <li><a class="active" href="#"><span>Catégorie</span> :
                                        {{ $produit->categorie->intitule }}</a></li>
                                <li><a href="#"><span>Disponibilité</span> : En stock</a></li>
                            </ul>
                            <p>Mill Oil is an innovative oil filled radiator with the most modern technology. If you are
                                looking
                                for
                                something that can make your interior look awesome, and at the same time give you the
                                pleasant
                                warm feeling
                                during the winter.</p>
                            <div class="product_count">
                                <label for="qty">Quantité:</label>
                                <input type="number" name="qty" id="sst" max="20"  min="1" value="1"
                                    title="Quantity:" class="input-text qty">
                                {{-- <button
                                onclick="var result = document.getElementById('sst'); var sst = result.value; if( !isNaN( sst )) result.value++;return false;"
                                class="increase items-count" type="button"><i class="lnr lnr-chevron-up"></i></button>
                            <button
                                onclick="var result = document.getElementById('sst'); var sst = result.value; if( !isNaN( sst ) &amp;&amp; sst > 0 ) result.value--;return false;"
                                class="reduced items-count" type="button"><i class="lnr lnr-chevron-down"></i></button> --}}
                            </div>
                            <div class="card_area d-flex align-items-center">
                                <input type="submit" class="btn btn-primary primary-btn" value="Ajouter">
                                <a class="icon_btn" href="#"><i class="lnr lnr lnr-diamond"></i></a>
                                <a class="icon_btn" href="#"><i class="lnr lnr lnr-heart"></i></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <section class="related-product-area section_gap_bottom">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6 text-center">
                    <div class="section-title">
                        <h1>Les meilleurs ventes</h1>
                        <p>Cette liste représente les meilleurs ventes réalisées aucours de la semaine. Découvrez
                            ce que d'autres personnes commandent.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-9">
                    <div class="row">
                        @foreach ($best_products as $item)
                            <div class="col-lg-4 col-md-4 col-sm-6 mb-20">
                                <div class="single-related-product d-flex">
                                    <a href="{{ route('single-product', $item->slug) }}"><img height="70"
                                            src="{{ asset('admin/img/products/' . $item->image) }}" alt=""></a>
                                    <div class="desc">
                                        <a href="{{ route('single-product', $item->slug) }}"
                                            class="title">{{ $item->intitule }}</a>
                                        <div class="price">
                                            <!--h6>$189.00</h6-->
                                            <h6 class="l-through">{{ $item->prix }} CFA</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach





                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="ctg-right">
                        <a href="#" target="_blank">
                            <img class="img-fluid d-block mx-auto" src="img/category/c5.jpg" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
