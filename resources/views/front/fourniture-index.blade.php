@extends('layouts.front.layout3')
@section('content')
    <div class="container-fluid page-header py-5 mb-5 wow fadeIn" data-wow-delay="0.1s"
        style="visibility: visible; animation-delay: 0.1s; animation-name: fadeIn;">
        <div class="container py-5">
            <h1 class="display-1 text-white  slideInDown">Fourniture</h1>
           {{--  <nav aria-label="breadcrumb animated slideInDown">
                <ol class="breadcrumb text-uppercase mb-0">
                    <li class="breadcrumb-item"><a class="text-white" href="{{ route('home') }}">Accueil</a></li>
                    <li class="breadcrumb-item"><a class="text-white" href="#">Pages</a></li>
                    <li class="breadcrumb-item text-primary active" aria-current="page">Store</li>
                </ol>
            </nav> --}}
        </div>
    </div>

     <!-- About Start -->
     <div class="container-xxl py-5">
        <div class="row container">
            <div class="col-xl-3 col-lg-4 col-md-5">
                <div class="sidebar-categories">
                    <div class="head">Catégories</div>
                    <ul class="main-categories">
                        @foreach ($categories as $item)
                            <li class="main-nav-list"><a data-toggle="collapse" href="#fruitsVegetable"
                                    aria-expanded="false" aria-controls="fruitsVegetable"><span
                                        class="lnr lnr-arrow-right"></span>{{ $item->intitule }}<span
                                        class="number">(5)</span></a>
                                <ul class="collapse" id="fruitsVegetable" data-toggle="collapse" aria-expanded="false"
                                    aria-controls="fruitsVegetable">
                                    <li class="main-nav-list child"><a href="#">Frozen Fish<span
                                                class="number">(13)</span></a></li>
                                    <li class="main-nav-list child"><a href="#">Dried Fish<span
                                                class="number">(09)</span></a></li>
                                    <li class="main-nav-list child"><a href="#">Fresh Fish<span
                                                class="number">(17)</span></a></li>
                                    <li class="main-nav-list child"><a href="#">Meat Alternatives<span
                                                class="number">(01)</span></a></li>
                                    <li class="main-nav-list child"><a href="#">Meat<span
                                                class="number">(11)</span></a>
                                    </li>
                                </ul>
                            </li>
                        @endforeach
                    </ul>
                </div>
                {{--   <div class="sidebar-filter mt-50">
                    <div class="top-filter-head">Product Filters</div>
                    <div class="common-filter">
                        <div class="head">Brands</div>
                        <form action="#">
                            <ul>
                                <li class="filter-list"><input class="pixel-radio" type="radio" id="apple"
                                        name="brand"><label for="apple">Apple<span>(29)</span></label></li>
                                <li class="filter-list"><input class="pixel-radio" type="radio" id="asus"
                                        name="brand"><label for="asus">Asus<span>(29)</span></label></li>
                                <li class="filter-list"><input class="pixel-radio" type="radio" id="gionee"
                                        name="brand"><label for="gionee">Gionee<span>(19)</span></label></li>
                                <li class="filter-list"><input class="pixel-radio" type="radio" id="micromax"
                                        name="brand"><label for="micromax">Micromax<span>(19)</span></label></li>
                                <li class="filter-list"><input class="pixel-radio" type="radio" id="samsung"
                                        name="brand"><label for="samsung">Samsung<span>(19)</span></label></li>
                            </ul>
                        </form>
                    </div>
                    <div class="common-filter">
                        <div class="head">Color</div>
                        <form action="#">
                            <ul>
                                <li class="filter-list"><input class="pixel-radio" type="radio" id="black"
                                        name="color"><label for="black">Black<span>(29)</span></label></li>
                                <li class="filter-list"><input class="pixel-radio" type="radio" id="balckleather"
                                        name="color"><label for="balckleather">Black
                                        Leather<span>(29)</span></label></li>
                                <li class="filter-list"><input class="pixel-radio" type="radio" id="blackred"
                                        name="color"><label for="blackred">Black
                                        with red<span>(19)</span></label></li>
                                <li class="filter-list"><input class="pixel-radio" type="radio" id="gold"
                                        name="color"><label for="gold">Gold<span>(19)</span></label></li>
                                <li class="filter-list"><input class="pixel-radio" type="radio" id="spacegrey"
                                        name="color"><label for="spacegrey">Spacegrey<span>(19)</span></label></li>
                            </ul>
                        </form>
                    </div>
                    <div class="common-filter">
                        <div class="head">Price</div>
                        <div class="price-range-area">
                            <div id="price-range"></div>
                            <div class="value-wrapper d-flex">
                                <div class="price">Price:</div>
                                <span>$</span>
                                <div id="lower-value"></div>
                                <div class="to">to</div>
                                <span>$</span>
                                <div id="upper-value"></div>
                            </div>
                        </div>
                    </div>
                </div> --}}
            </div>
            <div class="col-xl-9 col-lg-8 col-md-7">
                <!-- Start Filter Bar --
                    <div class="filter-bar d-flex flex-wrap align-items-center">
                        <div class="sorting">
                            <select class="form-select">
                                <option value="1">Default sorting</option>
                                <option value="1">Default sorting</option>
                                <option value="1">Default sorting</option>
                            </select>
                        </div>
                        <div class="sorting mr-auto">
                            <select class="form-select">
                                <option value="1">Show 12</option>
                                <option value="1">Show 12</option>
                                <option value="1">Show 12</option>
                            </select>
                        </div>
                        <div class="pagination">
                            <a href="#" class="prev-arrow"><i class="fa fa-long-arrow-left"
                                    aria-hidden="true"></i></a>
                            <a href="#" class="active">1</a>
                            <a href="#">2</a>
                            <a href="#">3</a>
                            <a href="#" class="dot-dot"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a>
                            <a href="#">6</a>
                            <a href="#" class="next-arrow"><i class="fa fa-long-arrow-right"
                                    aria-hidden="true"></i></a>
                        </div>
                    </div-->
                <!-- End Filter Bar -->
                <!-- Start Best Seller -->
                <section class="lattest-product-area pb-40 category-list">
                    @if (Session::get('data'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{{(Session::get('data'))}} a été bien ajouté au panier !</strong> <a href="{{route('panier.show')}}">Voir Panier</a>.
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    @endif
                  

                    <div class="row">
                        @foreach ($produits as $item)
                            <!-- single product -->
                            <div class="col-lg-4 col-md-6">
                                <a href="{{ route('single-product', $item->slug) }}">
                                    <div class="single-product text-center">
                                        <img class="img-fluid" height="100"
                                            src=" {{ asset('admin/img/products/' . $item->image) }}" alt="">
                                        <div class="product-details">
                                            <h6>{{ $item->intitule }}</h6>
                                            <div class="price">
                                                <h5>{{ $item->prix }} CFA</h5>
                                            </div>
                                            
                                           {{--  <div class="prd-bottom">
                                                <a href="{{ route('ajout-commande', [$item->id, 1]) }}"
                                                    class="social-info btn btn-primary py-0 px-4">
                                                    <span class="ti-bag"></span>
                                                    <p class="hover-text">Ajouter</p>
                                                </a>
                                                <a href="{{ route('single-product', $item->slug) }}" class="social-info">
                                                    <span class="lnr lnr-move"></span>
                                                    <p class="hover-text">Détails</p>
                                                </a>
                                            </div> --}}
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <!-- single product -->
                        @endforeach

                    </div>
                </section>
                <!-- End Best Seller -->
                <!-- Start Filter Bar -->
                <div class="filter-bar d-flex flex-wrap align-items-center">
                    <div class="sorting mr-auto">
                        <select>
                            <option value="1">Show 12</option>
                            <option value="1">Show 12</option>
                            <option value="1">Show 12</option>
                        </select>
                    </div>
                    <div class="pagination">
                        <a href="#" class="prev-arrow"><i class="fa fa-long-arrow-left"
                                aria-hidden="true"></i></a>
                        <a href="#" class="active">1</a>
                        <a href="#">2</a>
                        <a href="#">3</a>
                        <a href="#" class="dot-dot"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a>
                        <a href="#">6</a>
                        <a href="#" class="next-arrow"><i class="fa fa-long-arrow-right"
                                aria-hidden="true"></i></a>
                    </div>
                </div>
                <!-- End Filter Bar -->
            </div>
        </div>
    </div>
    <!-- About End -->
@endsection