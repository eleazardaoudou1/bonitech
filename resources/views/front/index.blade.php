@extends('layouts.front.layout1')

@section('content')

    <!-- Carousel Start -->
    <div class="container-fluid p-0 pb-5 wow fadeIn" data-wow-delay="0.1s">
        <div class="owl-carousel header-carousel position-relative">
            <div class="owl-carousel-item position-relative" data-dot="<img src='{{asset('front/img/carousel-1.jpg') }}'>">
                <img class="img-fluid" src= {{asset('front/img/carousel-1.jpg') }} alt="">
                <div class="owl-carousel-inner">
                    <div class="container">
                        <div class="row justify-content-start">
                            <div class="col-10 col-lg-8">
                                <h1 class="display-1 text-white animated slideInDown">Architecture, Design et Construction</h1>
                                <p class="fs-5 fw-medium text-white mb-4 pb-3">Réalisez tous vos projets d'architecture avec nous.</p>
                                <a href="" class="btn btn-primary py-3 px-5 animated slideInLeft">En savoir plus</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="owl-carousel-item position-relative" data-dot="<img src='{{asset('front/img/carousel-2.jpg') }}'>">
                <img class="img-fluid" src={{asset('front/img/carousel-2.jpg') }} alt="">
                <div class="owl-carousel-inner">
                    <div class="container">
                        <div class="row justify-content-start">
                            <div class="col-10 col-lg-8">
                                <h1 class="display-1 text-white animated slideInDown">Donnez vie à vos projets de construction</h1>
                                <p class="fs-5 fw-medium text-white mb-4 pb-3">Nous réalisons vos projets de construction avec le plus grand soin.</p>
                                <a href="" class="btn btn-primary py-3 px-5 animated slideInLeft">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="owl-carousel-item position-relative" data-dot="<img src='{{asset('front/img/carousel-3.jpg') }}'>">
                <img class="img-fluid" src={{asset('front/img/carousel-3.jpg') }} alt="">
                <div class="owl-carousel-inner">
                    <div class="container">
                        <div class="row justify-content-start">
                            <div class="col-10 col-lg-8">
                                <h1 class="display-1 text-white animated slideInDown">Optez pour des plans et des fournitures hors-pairs</h1>
                                <p class="fs-5 fw-medium text-white mb-4 pb-3">Nous vous proposons des plans et des fournitures adaptés à vos goûts pour votre satisfaction.</p>
                                <a href="" class="btn btn-primary py-3 px-5 animated slideInLeft">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Carousel End -->


    <!-- Facts Start -->
    <div class="container-xxl py-5">
        <div class="container pt-5">
            <div class="row g-4">
                <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                    <div class="fact-item text-center bg-light h-100 p-5 pt-0">
                        <div class="fact-icon">
                            <img src= {{asset('front/img/icons/icon-2.png') }} alt="Icon">
                        </div>
                        <h3 class="mb-3">Construction</h3>
                        <p class="mb-0">Nous étudions et analysons votre projet de construction. Nous mettons en place toute une équipe d'experts pour le réaliser.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
                    <div class="fact-item text-center bg-light h-100 p-5 pt-0">
                        <div class="fact-icon">
                            <img src={{asset('front/img/icons/icon-3.png') }} alt="Icon">
                        </div>
                        <h3 class="mb-3">Solutions Innovantes</h3>
                        <p class="mb-0">Notre leit-motiv est de proposer des solutions innovantes qui apportent des solutions parfaitement adaptées aux besoin de nos clients.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.5s">
                    <div class="fact-item text-center bg-light h-100 p-5 pt-0">
                        <div class="fact-icon">
                            <img src={{asset('front/img/icons/icon-4.png') }} alt="Icon">
                        </div>
                        <h3 class="mb-3">Respect des délais</h3>
                        <p class="mb-0">Fournir et livrer vos projets à tant a toujours été au centre de nos motivations.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Facts End -->


    <!-- About Start -->
    <div class="container-xxl py-5">
        <div class="container">
            <div class="row g-5">
                <div class="col-lg-6 wow fadeIn" data-wow-delay="0.1s">
                    <div class="about-img">
                        <img class="img-fluid" src={{asset('front/img/about-1.jpg') }} alt="">
                        <img class="img-fluid" src={{asset('front/img/about-2.jpg') }} alt="">
                    </div>
                </div>
                <div class="col-lg-6 wow fadeIn" data-wow-delay="0.5s">
                    <h4 class="section-title">A propos de nous</h4>
                    <h1 class="display-5 mb-4">Une agence d'architecture créative et engagée pour construire votre maison de rêve</h1>
                    <p>BoniTech ets une grande entreprise d'architecture qui propose des plans de construction et réalise des projets de construction
                        pour ses clients. Nous sommes spécialisés en architecture et en fournitures de matériaux.
                    </p>
                    <p class="mb-4">A BoniTech, nous nous efforçons pour vous offrir un service de première classe. Nous vous aidons à donner 
                        vie à vos projets depuis l'idée jusqu'à la construction y compris la fourninture totale. Nous vous attendons, contactez-nous
                    et démarrons une belle aventure ensemble.</p>
                    <div class="d-flex align-items-center mb-5">
                        <div class="d-flex flex-shrink-0 align-items-center justify-content-center border border-5 border-primary" style="width: 120px; height: 120px;">
                            <h1 class="display-1 mb-n2" data-toggle="counter-up">10</h1>
                        </div>
                        <div class="ps-4">
                            <h3>Années</h3>
                            <h3>Expérience</h3>
                            <h3 class="mb-0">de Réalisation</h3>
                        </div>
                    </div>
                    <!--a class="btn btn-primary py-3 px-5" href="">Read More</a-->
                </div>
            </div>
        </div>
    </div>
    <!-- About End -->


    <!-- Service Start -->
    <div class="container-xxl py-5">
        <div class="container">
            <div class="text-center mx-auto mb-5 wow fadeInUp" data-wow-delay="0.1s" style="max-width: 600px;">
                <h4 class="section-title">Nos Services</h4>
                <h1 class="display-5 mb-4">Nous sommes axés sur l'architecture, le Design et la construction  et la fourniture.</h1>
            </div>
            <div class="row g-4">
                <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                    <div class="service-item d-flex position-relative text-center h-100">
                        <img class="bg-img" src={{asset('front/img/service-1.jpg') }} alt="">
                        <div class="service-text p-5">
                            <img class="mb-4" src={{asset('front/img/icons/icon-5.png') }} alt="Icon">
                            <h3 class="mb-3">Architecture</h3>
                            <p class="mb-4">La réalisation des plans adaptés à vos projets après une étude minutieuse. </p>
                            <a class="btn" href=""><i class="fa fa-plus text-primary me-3"></i>Read More</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
                    <div class="service-item d-flex position-relative text-center h-100">
                        <img class="bg-img" src={{asset('front/img/service-2.jpg') }}  alt="">
                        <div class="service-text p-5">
                            <img class="mb-4" src={{asset('front/img/icons/icon-6.png') }} alt="Icon">
                            <h3 class="mb-3">3D Animation</h3>
                            <p class="mb-4">Nous concevons en trois dimensions avec de la haute définition le rendu de votre maison.</p>
                            <a class="btn" href=""><i class="fa fa-plus text-primary me-3"></i>Read More</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.5s">
                    <div class="service-item d-flex position-relative text-center h-100">
                        <img class="bg-img" src={{asset('front/img/service-3.jpg') }}alt="">
                        <div class="service-text p-5">
                            <img class="mb-4" src={{asset('front/img/icons/icon-7.png') }} alt="Icon">
                            <h3 class="mb-3">Réalisation de plans de construction</h3>
                            <p class="mb-4">Lorsqu'ils s'agit de concevoir des plans pour vos projets de contructions, nous sommes l'équipe qu'il vous faut.</p>
                            <a class="btn" href=""><i class="fa fa-plus text-primary me-3"></i>Read More</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                    <div class="service-item d-flex position-relative text-center h-100">
                        <img class="bg-img" src={{asset('front/img/service-4.jpg') }} alt="">
                        <div class="service-text p-5">
                            <img class="mb-4" src={{asset('front/img/icons/icon-8.png') }} alt="Icon">
                            <h3 class="mb-3"> Design Intérieur</h3>
                            <p class="mb-4">Nous designons le beau et l'esthétique. Nous choisissons des meubles de décoration qui coupent le souffle.</p>
                            <a class="btn" href=""><i class="fa fa-plus text-primary me-3"></i>Read More</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
                    <div class="service-item d-flex position-relative text-center h-100">
                        <img class="bg-img" src={{asset('front/img/service-5.jpg') }} alt="">
                        <div class="service-text p-5">
                            <img class="mb-4" src={{asset('front/img/icons/icon-9.png') }} alt="Icon">
                            <h3 class="mb-3">Domotique</h3>
                            <p class="mb-4">Transformer votre maison en un endroit connecté, nous savons le faire.</p>
                            <a class="btn" href=""><i class="fa fa-plus text-primary me-3"></i>Read More</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.5s">
                    <div class="service-item d-flex position-relative text-center h-100">
                        <img class="bg-img" src={{asset('front/img/service-6.jpg') }}  alt="">
                        <div class="service-text p-5">
                            <img class="mb-4" src={{asset('front/img/icons/icon-10.png') }} alt="Icon">
                            <h3 class="mb-3">Fourniture</h3>
                            <p class="mb-4">Nous disposons des équipements nécessaires pour la fourniture de vos chambres après construction.</p>
                            <a class="btn" href=""><i class="fa fa-plus text-primary me-3"></i>Read More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Service End -->


    <!-- Feature Start -->
    <div class="container-xxl py-5">
        <div class="container">
            <div class="row g-5">
                <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.1s">
                    <h4 class="section-title">Pourquoi nous choisir!</h4>
                    <h1 class="display-5 mb-4">Pourquoi vous devez nous confier vos projets? En quoi sommes-nous différents ?</h1>
                    <p class="mb-4">A BoniTech, nous mettons le client au centre de nos efforts. Nous travaillons pour le client. Nous travaillons avec le client. Nous respectons le client et nous sommes 
                        engagés à l'aider à réaliser son projet quelque soit son budget.
                    </p>
                    <div class="row g-4">
                        <div class="col-12">
                            <div class="d-flex align-items-start">
                                <img class="flex-shrink-0" src={{asset('front/img/icons/icon-2.png') }} alt="Icon">
                                <div class="ms-4">
                                    <h3>Architecture</h3>
                                    <p class="mb-0">La réalisation des plans adaptés à vos projets après une étude minutieuse.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="d-flex align-items-start">
                                <img class="flex-shrink-0" src={{asset('front/img/icons/icon-3.png') }} alt="Icon">
                                <div class="ms-4">
                                    <h3>Solutions Innovantes</h3>
                                    <p class="mb-0">Innovez avec nous dans la réalisation de vos projets.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="d-flex align-items-start">
                                <img class="flex-shrink-0" src={{asset('front/img/icons/icon-4.png') }} alt="Icon">
                                <div class="ms-4">
                                    <h3>Délais respectés</h3>
                                    <p class="mb-0">Nous savons combien il est important pour nos clients d'être livrés à tant. A BoniTech, nous nous efforçons pour respecter les délais.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.5s">
                    <div class="feature-img">
                        <img class="img-fluid" src={{asset('front/img/about-2.jpg') }}  alt="">
                        <img class="img-fluid" src={{asset('front/img/about-1.jpg') }}  alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Feature End -->


    <!-- Project Start -->
    <div class="container-xxl project py-5">
        <div class="container">
            <div class="text-center mx-auto mb-5 wow fadeInUp" data-wow-delay="0.1s" style="max-width: 600px;">
                <h4 class="section-title">Nos Projets</h4>
                <h1 class="display-5 mb-4">Explorez nos derniers projets innovants. </h1>
            </div>
            <div class="row g-4 wow fadeInUp" data-wow-delay="0.3s">
                <div class="col-lg-4">
                    <div class="nav nav-pills d-flex justify-content-between w-100 h-100 me-4">
                        <button class="nav-link w-100 d-flex align-items-center text-start p-4 mb-4 active" data-bs-toggle="pill" data-bs-target="#tab-pane-1" type="button">
                            <h3 class="m-0">01. Villa à Calavi</h3>
                        </button>
                        <button class="nav-link w-100 d-flex align-items-center text-start p-4 mb-4" data-bs-toggle="pill" data-bs-target="#tab-pane-2" type="button">
                            <h3 class="m-0">02. Villa 5 * à Arconville</h3>
                        </button>
                        <button class="nav-link w-100 d-flex align-items-center text-start p-4 mb-4" data-bs-toggle="pill" data-bs-target="#tab-pane-3" type="button">
                            <h3 class="m-0">03. Duplex 5* </h3>
                        </button>
                        <button class="nav-link w-100 d-flex align-items-center text-start p-4 mb-0" data-bs-toggle="pill" data-bs-target="#tab-pane-4" type="button">
                            <h3 class="m-0">04. Equipements de villa</h3>
                        </button>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="tab-content w-100">
                        <div class="tab-pane fade show active" id="tab-pane-1">
                            <div class="row g-4">
                                <div class="col-md-6" style="min-height: 350px;">
                                    <div class="position-relative h-100">
                                        <img class="position-absolute img-fluid w-100 h-100" src={{asset('front/img/project-1.jpg') }}
                                            style="object-fit: cover;" alt="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <h1 class="mb-3">10 Années d'expérience dans l'architecture et l'industrie du design intérieur</h1>
                                    <p class="mb-4">Ces projets ont été réalisés par nos experts avec le plus grand soin. Et nous vous les présentons.</p>
                                    <p><i class="fa fa-check text-primary me-3"></i>Satisfaction du client</p>
                                    <p><i class="fa fa-check text-primary me-3"></i>Délais respectés</p>
                                    <p><i class="fa fa-check text-primary me-3"></i>Professionalisme</p>
                                    <!--a href="" class="btn btn-primary py-3 px-5 mt-3">Read More</a-->
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab-pane-2">
                            <div class="row g-4">
                                <div class="col-md-6" style="min-height: 350px;">
                                    <div class="position-relative h-100">
                                        <img class="position-absolute img-fluid w-100 h-100" src="img/project-2.jpg"
                                            style="object-fit: cover;" alt="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <h1 class="mb-3">10 Années d'expérience dans l'architecture et l'industrie du design intérieur</h1>
                                    <p class="mb-4">Ces projets ont été réalisés par nos experts avec le plus grand soin. Et nous vous les présentons.</p>
                                    <p><i class="fa fa-check text-primary me-3"></i>Satisfaction du client</p>
                                    <p><i class="fa fa-check text-primary me-3"></i>Délais respectés</p>
                                    <p><i class="fa fa-check text-primary me-3"></i>Professionalisme</p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab-pane-3">
                            <div class="row g-4">
                                <div class="col-md-6" style="min-height: 350px;">
                                    <div class="position-relative h-100">
                                        <img class="position-absolute img-fluid w-100 h-100" src="img/project-3.jpg"
                                            style="object-fit: cover;" alt="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <h1 class="mb-3">10 Années d'expérience dans l'architecture et l'industrie du design intérieur</h1>
                                    <p class="mb-4">Ces projets ont été réalisés par nos experts avec le plus grand soin. Et nous vous les présentons.</p>
                                    <p><i class="fa fa-check text-primary me-3"></i>Satisfaction du client</p>
                                    <p><i class="fa fa-check text-primary me-3"></i>Délais respectés</p>
                                    <p><i class="fa fa-check text-primary me-3"></i>Professionalisme</p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab-pane-4">
                            <div class="row g-4">
                                <div class="col-md-6" style="min-height: 350px;">
                                    <div class="position-relative h-100">
                                        <img class="position-absolute img-fluid w-100 h-100" src="img/project-4.jpg"
                                            style="object-fit: cover;" alt="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <h1 class="mb-3">10 Années d'expérience dans l'architecture et l'industrie du design intérieur</h1>
                                    <p class="mb-4">Ces projets ont été réalisés par nos experts avec le plus grand soin. Et nous vous les présentons.</p>
                                    <p><i class="fa fa-check text-primary me-3"></i>Satisfaction du client</p>
                                    <p><i class="fa fa-check text-primary me-3"></i>Délais respectés</p>
                                    <p><i class="fa fa-check text-primary me-3"></i>Professionalisme</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Project End -->


    <!-- Team Start -->
    <!--div class="container-xxl py-5">
        <div class="container">
            <div class="text-center mx-auto mb-5 wow fadeInUp" data-wow-delay="0.1s" style="max-width: 600px;">
                <h4 class="section-title">Team Members</h4>
                <h1 class="display-5 mb-4">We Are Creative Architecture Team For Your Dream Home</h1>
            </div>
            <div class="row g-0 team-items">
                <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                    <div class="team-item position-relative">
                        <div class="position-relative">
                            <img class="img-fluid" src="img/team-1.jpg" alt="">
                            <div class="team-social text-center">
                                <a class="btn btn-square" href=""><i class="fab fa-facebook-f"></i></a>
                                <a class="btn btn-square" href=""><i class="fab fa-twitter"></i></a>
                                <a class="btn btn-square" href=""><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                        <div class="bg-light text-center p-4">
                            <h3 class="mt-2">Architect Name</h3>
                            <span class="text-primary">Designation</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
                    <div class="team-item position-relative">
                        <div class="position-relative">
                            <img class="img-fluid" src="img/team-2.jpg" alt="">
                            <div class="team-social text-center">
                                <a class="btn btn-square" href=""><i class="fab fa-facebook-f"></i></a>
                                <a class="btn btn-square" href=""><i class="fab fa-twitter"></i></a>
                                <a class="btn btn-square" href=""><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                        <div class="bg-light text-center p-4">
                            <h3 class="mt-2">Architect Name</h3>
                            <span class="text-primary">Designation</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.5s">
                    <div class="team-item position-relative">
                        <div class="position-relative">
                            <img class="img-fluid" src="img/team-3.jpg" alt="">
                            <div class="team-social text-center">
                                <a class="btn btn-square" href=""><i class="fab fa-facebook-f"></i></a>
                                <a class="btn btn-square" href=""><i class="fab fa-twitter"></i></a>
                                <a class="btn btn-square" href=""><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                        <div class="bg-light text-center p-4">
                            <h3 class="mt-2">Architect Name</h3>
                            <span class="text-primary">Designation</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.7s">
                    <div class="team-item position-relative">
                        <div class="position-relative">
                            <img class="img-fluid" src="img/team-4.jpg" alt="">
                            <div class="team-social text-center">
                                <a class="btn btn-square" href=""><i class="fab fa-facebook-f"></i></a>
                                <a class="btn btn-square" href=""><i class="fab fa-twitter"></i></a>
                                <a class="btn btn-square" href=""><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                        <div class="bg-light text-center p-4">
                            <h3 class="mt-2">Architect Name</h3>
                            <span class="text-primary">Designation</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div-->
    <!-- Team End -->


    <!-- Appointment Start -->
    <div class="container-xxl py-5">
        <div class="container">
            <div class="row g-5">
                <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.1s">
                    <h4 class="section-title">Prenez RDV avec nous.</h4>
                    <h1 class="display-5 mb-4">Rencontrez-nous et discutons de votre projet. </h1>
                    <p class="mb-4">Contactez-nous sans plus attendre. Nous sommes impatients de donner vie à votre projet. Nos équipes n'attendent que vous.</p>
                    <div class="row g-4">
                        <div class="col-12">
                            <div class="d-flex">
                                <div class="d-flex flex-shrink-0 align-items-center justify-content-center bg-light" style="width: 65px; height: 65px;">
                                    <i class="fa fa-2x fa-phone-alt text-primary"></i>
                                </div>
                                <div class="ms-4">
                                    <p class="mb-2">Appelez nous</p>
                                    <h3 class="mb-0">+229 674 048 93 / 415 480 54</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="d-flex">
                                <div class="d-flex flex-shrink-0 align-items-center justify-content-center bg-light" style="width: 65px; height: 65px;">
                                    <i class="fa fa-2x fa-envelope-open text-primary"></i>
                                </div>
                                <div class="ms-4">
                                    <p class="mb-2">Adresse Mail</p>
                                    <h3 class="mb-0">bonitech3dcorporation@gmail.com</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.5s">
                    <div class="row g-3">
                        <div class="col-12 col-sm-6">
                            <input type="text" class="form-control" placeholder="Votre nom" style="height: 55px;">
                        </div>
                        <div class="col-12 col-sm-6">
                            <input type="email" class="form-control" placeholder="Votre adresse mail" style="height: 55px;">
                        </div>
                        <div class="col-12 col-sm-6">
                            <input type="text" class="form-control" placeholder="Votre contact" style="height: 55px;">
                        </div>
                        <div class="col-12 col-sm-6">
                            <select class="form-select" style="height: 55px;">
                                <option selected>Motif</option>
                                <option value="1">Demande de devis</option>
                                <option value="2">Assistance</option>
                                <option value="3">Plainte</option>
                            </select>
                        </div>
                       {{--  <div class="col-12 col-sm-6">
                            <div class="date" id="date" data-target-input="nearest">
                                <input type="text"
                                    class="form-control datetimepicker-input"
                                    placeholder="Choose Date" data-target="#date" data-toggle="datetimepicker" style="height: 55px;">
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="time" id="time" data-target-input="nearest">
                                <input type="text"
                                    class="form-control datetimepicker-input"
                                    placeholder="Choose Date" data-target="#time" data-toggle="datetimepicker" style="height: 55px;">
                            </div>
                        </div> --}}
                        <div class="col-12">
                            <textarea class="form-control" rows="5" placeholder="Message"></textarea>
                        </div>
                        <div class="col-12">
                            <button class="btn btn-primary w-100 py-3" type="submit">Prendre Rendez-Vous</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Appointment End -->


    <!-- Testimonial Start -->
    <div class="container-xxl py-5">
        <div class="container">
            <div class="text-center mx-auto mb-5 wow fadeInUp" data-wow-delay="0.1s" style="max-width: 600px;">
                <h4 class="section-title">Témoignages de nos clients</h4>
                <h1 class="display-5 mb-4">Ces clients témoignent pour nous</h1>
            </div>
            <div class="owl-carousel testimonial-carousel wow fadeInUp" data-wow-delay="0.1s">
                <div class="testimonial-item text-center" data-dot="<img class='img-fluid' src='img/testimonial-1.jpg' alt=''>">
                    <p class="fs-5">Clita clita tempor justo dolor ipsum amet kasd amet duo justo duo duo labore sed sed. Magna ut diam sit et amet stet eos sed clita erat magna elitr erat sit sit erat at rebum justo sea clita.</p>
                    <h3>Client Name</h3>
                    <span class="text-primary">Profession</span>
                </div>
                <div class="testimonial-item text-center" data-dot="<img class='img-fluid' src='img/testimonial-2.jpg' alt=''>">
                    <p class="fs-5">Clita clita tempor justo dolor ipsum amet kasd amet duo justo duo duo labore sed sed. Magna ut diam sit et amet stet eos sed clita erat magna elitr erat sit sit erat at rebum justo sea clita.</p>
                    <h3>Client Name</h3>
                    <span class="text-primary">Profession</span>
                </div>
                <div class="testimonial-item text-center" data-dot="<img class='img-fluid' src='img/testimonial-3.jpg' alt=''>">
                    <p class="fs-5">Clita clita tempor justo dolor ipsum amet kasd amet duo justo duo duo labore sed sed. Magna ut diam sit et amet stet eos sed clita erat magna elitr erat sit sit erat at rebum justo sea clita.</p>
                    <h3>Client Name</h3>
                    <span class="text-primary">Profession</span>
                </div>
            </div>      
        </div>
    </div>
    <!-- Testimonial End -->


    
@endsection