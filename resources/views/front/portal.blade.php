@extends('layouts.front.layout')
@section('content')
    <!-- Carousel Start -->
    <div class="container-fluid p-0 pb-5 wow fadeIn" data-wow-delay="0.1s">
        <div class="owl-carousel header-carousel position-relative">
            <div class="owl-carousel-item position-relative" data-dot="<img src='{{ asset('front/img/carousel-1.jpg') }}'>">
                <img class="img-fluid" src={{ asset('front/img/portal-1.jpg') }} alt="">
                <div class="owl-carousel-inner">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-6">
                                <h1 class="display-4 text-white animated slideInDown">Architecture et Construction
                                </h1>
                                <a href="{{ route('home') }}" target="_blank"
                                    class="btn btn-primary py-3 px-5 animated slideInLeft">Voir</a>
                            </div>
                            <div class="col-6 justify-content-end">
                                <h1 class="display-4 text-white animated slideInDown">Fourniture et Design intérieur
                                </h1>
                                <a href="{{route('fourniture.index')}}" target="_blank"
                                class="btn btn-primary py-3 px-5 animated slideInLeft">Voir</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Carousel End -->
@endsection
