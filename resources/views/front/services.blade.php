@extends('layouts.front.layout1')
@section('content')
    <div class="container-fluid page-header py-5 mb-5 wow fadeIn" data-wow-delay="0.1s"
        style="visibility: visible; animation-delay: 0.1s; animation-name: fadeIn;">
        <div class="container py-5">
            <h1 class="display-1 text-white  slideInDown">Nos Services</h1>
            <nav aria-label="breadcrumb animated slideInDown">
                <ol class="breadcrumb text-uppercase mb-0">
                    <li class="breadcrumb-item"><a class="text-white" href="{{ route('home') }}">Accueil</a></li>
                    <li class="breadcrumb-item"><a class="text-white" href="#">Pages</a></li>
                    <li class="breadcrumb-item text-primary active" aria-current="page">Nos Services</li>
                </ol>
            </nav>
        </div>
    </div>

    <!-- About Start -->
    <div class="container-xxl py-5">
        <div class="container">
            <div class="row g-5">
                <div class="col-lg-6 wow fadeIn" data-wow-delay="0.1s">
                    <div class="about-img">
                        <img class="img-fluid" src={{ asset('front/img/about-1.jpg') }} alt="">
                        <img class="img-fluid" src={{ asset('front/img/about-2.jpg') }} alt="">
                    </div>
                </div>
                <div class="col-lg-6 wow fadeIn" data-wow-delay="0.5s">
                    <h4 class="section-title">A propos de nous</h4>
                    <h1 class="display-5 mb-4">Une agence d'architecture créative et engagée pour construire votre maison de
                        rêve</h1>
                    <p>BoniTech ets une grande entreprise d'architecture qui propose des plans de construction et réalise
                        des projets de construction
                        pour ses clients. Nous sommes spécialisés en architecture et en fournitures de matériaux.
                    </p>
                    <p class="mb-4">A BoniTech, nous nous efforçons pour vous offrir un service de première classe. Nous
                        vous aidons à donner
                        vie à vos projets depuis l'idée jusqu'à la construction y compris la fourninture totale. Nous vous
                        attendons, contactez-nous
                        et démarrons une belle aventure ensemble.</p>
                    <div class="d-flex align-items-center mb-5">
                        <div class="d-flex flex-shrink-0 align-items-center justify-content-center border border-5 border-primary"
                            style="width: 120px; height: 120px;">
                            <h1 class="display-1 mb-n2" data-toggle="counter-up">10</h1>
                        </div>
                        <div class="ps-4">
                            <h3>Années</h3>
                            <h3>Expérience</h3>
                            <h3 class="mb-0">de Réalisation</h3>
                        </div>
                    </div>
                    <!--a class="btn btn-primary py-3 px-5" href="">Read More</a-->
                </div>
            </div>

            <div class="row mb-5">
                <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
                    <div class="fact-item text-center bg-light h-100 p-5 pt-0">
                        <div class="fact-icon">
                            <img src={{ asset('front/img/icons/icon-3.png') }} alt="Icon">
                        </div>
                        <h3 class="mb-3">Architecture</h3>
                        <p class="mb-0"></p>
                    </div>
                </div>
                <div class="col-lg-8 col-md-6 my-auto">En architecture nous faisons la conception des plans architecturaux
                    avec suivi et réalisation. Nous développons également des concepts architectural, du patrimoine africain
                    et la création de meubles ...etc.
                </div>
            </div>

            <div class="row mb-5">
                <div class="col-lg-8 col-md-6 my-auto">
                    En design et produits nous nous efforçons de créer des concepts et faisons de la modélisation 3D des
                    objets, meubles, portes fenêtres et divers. Que nous nous efforçons de produire et proposons à nos
                    clients dans le projet de construction.
                </div>
                <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
                    <div class="fact-item text-center bg-light h-100 p-5 pt-0">
                        <div class="fact-icon">
                            <img src={{ asset('front/img/icons/icon-3.png') }} alt="Icon">
                        </div>
                        <h3 class="mb-3">Design et Produits</h3>
                        <p class="mb-0"></p>
                    </div>
                </div>
              
            </div>

            <div class="row">
                <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
                    <div class="fact-item text-center bg-light h-100 p-5 pt-0">
                        <div class="fact-icon">
                            <img src={{ asset('front/img/icons/icon-3.png') }} alt="Icon">
                        </div>
                        <h3 class="mb-3">Fournisseurs</h3>
                        <p class="mb-0"></p>
                    </div>
                </div>
                <div class="col-lg-8 col-md-6 my-auto">Dans les fournisseurs nous avons une équipe spécialisée dans le sourcing achat et livraison des tout produit et fournitures électronique, informatique et tout autres produits liés au domaine de l'architecture ou du génie civil de façon générale.
                </div>
            </div>


        </div>
    </div>
    <!-- About End -->
@endsection
