@extends('layouts.front.layout1')
@section('content')
<div class="container-fluid page-header py-5 mb-5 wow fadeIn" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeIn;">
    <div class="container py-5">
        <h1 class="display-1 text-white  slideInDown">Tableau de bord</h1>
        <nav aria-label="breadcrumb animated slideInDown">
            <div class="ext-uppercase text-white">Bienvenue, {{auth()->user()->username}}
            </div>  
            {{-- <ol class="breadcrumb text-uppercase mb-0">
                <li class="breadcrumb-item"><a class="text-white" href="{{route('home')}}">Accueil</a></li>
                <li class="breadcrumb-item"><a class="text-white" href="#">Pages</a></li>
                <li class="breadcrumb-item text-primary active" aria-current="page">A propos</li>
            </ol> --}}
        </nav>
    </div>
</div>

 
@endsection