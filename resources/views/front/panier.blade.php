@extends('layouts.front.layout1')
@section('content')
    <div class="container-fluid page-header py-5 mb-5 wow fadeIn" data-wow-delay="0.1s"
        style="visibility: visible; animation-delay: 0.1s; animation-name: fadeIn;">
        <div class="container py-5">
            <h1 class="display-1 text-white  slideInDown">Store</h1>
            <nav aria-label="breadcrumb animated slideInDown">
                <ol class="breadcrumb text-uppercase mb-0">
                    <li class="breadcrumb-item"><a class="text-white" href="{{ route('home') }}">Accueil</a></li>
                    <li class="breadcrumb-item"><a class="text-white" href="#">Pages</a></li>
                    <li class="breadcrumb-item text-white" aria-current="page">Store</li>
                    <li class="breadcrumb-item text-primary active" aria-current="page">Panier</li>
                </ol>
            </nav>
        </div>
    </div>
    <section class="cart_area">
        <div class="container">



            @if (Session::has('commande'))
                <div class="cart_inner">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Produit</th>
                                    <th scope="col"></th>
                                    <th scope="col">Prix</th>
                                    <th scope="col">Quantité</th>
                                    <th scope="col">Total</th>
                                    <th scope="col">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 0; ?>
                                @foreach (session('commande')->getItems() as $hash => $item)
                                    <tr id="<?php echo $i; ?>">
                                        <td>
                                            <a href="{{ route('single-product', $item->getExtraInfo('details.slug')) }}">
                                                <div class="media">
                                                    <div class="d-flex">
                                                        <img height="100"
                                                            src="{{ asset('admin/img/products/' . $item->getExtraInfo('details.image')) }} "
                                                            alt="">
                                                    </div>
                                                    <div class="media-body">
                                                        <p>{{ $item->getDetails()->title }}</p>
                                                    </div>
                                                </div>
                                            </a>

                                        </td>
                                        <td></td>
                                        <td>
                                            <h5>{{ $item->getDetails()->price }} CFA</h5>
                                        </td>


                                        <td>
                                            <div class="product_count">
                                                <input type="number" name="qty" id="sst" maxlength="12"
                                                    value="{{ $item->getDetails()->quantity }}" title="Quantity:"
                                                    class="input-text qty form-control">
                                            </div>
                                        </td>
                                        <td>

                                            <h5>{{ $item->getDetails()->total_price }} CFA</h5>
                                        </td>
                                        <td>
                                            <button class="btn btn-primary"
                                                onclick="updateItem(<?php echo $i; ?> , 45  )">Mettre
                                                à jour</button>
                                        </td>

                                    </tr>
                                    <?php $i++; ?>
                                @endforeach

                                <tr class="bottom_button">
                                    <td>
                                        <a class="gray_btn" href="#">Mettre à jour
                                            Panier</a>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    <td>
                                    </td>
                                    <td>
                                        <a class="gray_btn" href="{{ route('panier.destroy') }}">Vider Panier</a>
                                    </td>

                                </tr>
                                <tr>
                                    <td>

                                    </td>
                                    <td>
                                    <td></td>
                                    </td>
                                    <td>
                                        <h5>Sous-total</h5>
                                    </td>
                                    <td>
                                        <h5>{{ session('commande')->getSubTotal() }}</h5>
                                    </td>
                                </tr>

                                <tr class="out_button_area">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <div class="checkout_btn_inner- d-flex align-items-center">
                                            <a class="btn btn-primary mx-1" href="{{ route('store') }}">Continuer Achat</a>
                                            <a class="btn btn-primary" href="{{ route('paiement') }}">Accéder au
                                                Paiement</a>
                                        </div>

                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            @else
                <h3 class="title_confirmation">Oups. Votre panier est vide !</h3>
                <a class="btn btn-primary" href="{{ route('store') }}">Retour sur la boutique</a>
            @endif
        </div>
    </section>


    <script>
        function updateItem(index, qty) {
            // processus ppour recuperer la valeur dans le input
            const fourthChild = document.getElementById("" + index + "").children.item(3);
            const ele = fourthChild.getElementsByClassName('product_count');
            const inp = ele[0].getElementsByTagName('input');
            console.log(inp[0].value)
            const valueInp = inp[0].value;
            //--------fin du processus

            //console.log(index)

            <?php
            $index = '<script>document.write(index)</script>';
            
            $i = 0;
            if (session('commande') != null) {
                foreach (session('commande')->getItems() as $item) {
                    if ($i == $index) {
                        session('commande')->updateItem($item->getHash(), [
                            'quantity' => '<script>document.write(valueInp)</script>',
                        ]);
                        //$item->getDetails()->quantity = '<script>document.write(valueInp)</script>';
                    }
                    $i++;
                }
            }
            
            //echo session('commande')->getItems()[$index];
            
            ?>
            //location.reload()

        }
    </script>
@endsection
