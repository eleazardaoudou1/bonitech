@extends('layouts.front.layout1')
@section('content')
<div class="container-fluid page-header py-5 mb-5 wow fadeIn" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeIn;">
    <div class="container py-5">
        <h1 class="display-1 text-white  slideInDown">Nos Projets</h1>
        <nav aria-label="breadcrumb animated slideInDown">
            <ol class="breadcrumb text-uppercase mb-0">
                <li class="breadcrumb-item"><a class="text-white" href="{{route('home')}}">Accueil</a></li>
                <li class="breadcrumb-item"><a class="text-white" href="#">Pages</a></li>
                <li class="breadcrumb-item text-primary active" aria-current="page">Nos Projets</li>
            </ol>
        </nav>
    </div>
</div>

  <!-- About Start -->
  <div class="container-xxl py-5">
    <div class="container">
        <div class="row g-5">
            <div class="col-lg-6 wow fadeIn" data-wow-delay="0.1s">
                <div class="about-img">
                    <img class="img-fluid" src={{asset('front/img/about-1.jpg') }} alt="">
                    <img class="img-fluid" src={{asset('front/img/about-2.jpg') }} alt="">
                </div>
            </div>
            <div class="col-lg-6 wow fadeIn" data-wow-delay="0.5s">
                <h4 class="section-title">A propos de nous</h4>
                <h1 class="display-5 mb-4">Une agence d'architecture créative et engagée pour construire votre maison de rêve</h1>
                <p>BoniTech ets une grande entreprise d'architecture qui propose des plans de construction et réalise des projets de construction
                    pour ses clients. Nous sommes spécialisés en architecture et en fournitures de matériaux.
                </p>
                <p class="mb-4">A BoniTech, nous nous efforçons pour vous offrir un service de première classe. Nous vous aidons à donner 
                    vie à vos projets depuis l'idée jusqu'à la construction y compris la fourninture totale. Nous vous attendons, contactez-nous
                et démarrons une belle aventure ensemble.</p>
                <div class="d-flex align-items-center mb-5">
                    <div class="d-flex flex-shrink-0 align-items-center justify-content-center border border-5 border-primary" style="width: 120px; height: 120px;">
                        <h1 class="display-1 mb-n2" data-toggle="counter-up">10</h1>
                    </div>
                    <div class="ps-4">
                        <h3>Années</h3>
                        <h3>Expérience</h3>
                        <h3 class="mb-0">de Réalisation</h3>
                    </div>
                </div>
                <!--a class="btn btn-primary py-3 px-5" href="">Read More</a-->
            </div>
        </div>
    </div>
</div>
<!-- About End -->

  <!-- Project Start -->
  <div class="container-xxl project py-5">
    <div class="container">
        <div class="text-center mx-auto mb-5 wow fadeInUp" data-wow-delay="0.1s" style="max-width: 600px;">
            <h4 class="section-title">Nos Projets</h4>
            <h1 class="display-5 mb-4">Explorez nos projets innovants. </h1>
        </div>
        <div class="row g-4 wow fadeInUp" data-wow-delay="0.3s">
            <div class="col-lg-4">
                <div class="nav nav-pills d-flex justify-content-between w-100 h-100 me-4">
                    <button class="nav-link w-100 d-flex align-items-center text-start p-4 mb-4 active" data-bs-toggle="pill" data-bs-target="#tab-pane-1" type="button">
                        <h3 class="m-0">01. VILLA HAUTE STANDING "N'DANIKOU"</h3>
                    </button>
                    <button class="nav-link w-100 d-flex align-items-center text-start p-4 mb-4" data-bs-toggle="pill" data-bs-target="#tab-pane-2" type="button">
                        <h3 class="m-0">02. Bungalow, Haut standing " Projet Délé</h3>
                    </button>
                    <button class="nav-link w-100 d-flex align-items-center text-start p-4 mb-4" data-bs-toggle="pill" data-bs-target="#tab-pane-3" type="button">
                        <h3 class="m-0">03. Villa SONOU </h3>
                    </button>
                    <button class="nav-link w-100 d-flex align-items-center text-start p-4 mb-0" data-bs-toggle="pill" data-bs-target="#tab-pane-4" type="button">
                        <h3 class="m-0">04. Autre villa</h3>
                    </button>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="tab-content w-100">
                    <div class="tab-pane fade show active" id="tab-pane-1">
                        <div class="row g-4">
                            <div class="col-md-6" style="min-height: 350px;">
                                <div class="position-relative h-100">
                                    <img class="position-absolute img-fluid w-100 h-100" src={{asset('front/img/projects/villa-nda.jpg') }}
                                        style="object-fit: cover;" alt="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h1 class="mb-3">10 Années d'expérience dans l'architecture et l'industrie du design intérieur</h1>
                                <p class="mb-4">Ces projets ont été réalisés par nos experts avec le plus grand soin. Et nous vous les présentons.</p>
                                <p><i class="fa fa-check text-primary me-3"></i>Satisfaction du client</p>
                                <p><i class="fa fa-check text-primary me-3"></i>Délais respectés</p>
                                <p><i class="fa fa-check text-primary me-3"></i>Professionalisme</p>
                                <!--a href="" 66 67 40 07 class="btn btn-primary py-3 px-5 mt-3">Read More</a-->
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab-pane-2">
                        <div class="row g-4">
                            <div class="col-md-6" style="min-height: 350px;">
                                <div class="position-relative h-100">
                                    <img class="position-absolute img-fluid w-100 h-100" src="{{asset('front/img/projects/bungalo.jpg') }}"
                                        style="object-fit: cover;" alt="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h1 class="mb-3">10 Années d'expérience dans l'architecture et l'industrie du design intérieur</h1>
                                <p class="mb-4">Ces projets ont été réalisés par nos experts avec le plus grand soin. Et nous vous les présentons.</p>
                                <p><i class="fa fa-check text-primary me-3"></i>Satisfaction du client</p>
                                <p><i class="fa fa-check text-primary me-3"></i>Délais respectés</p>
                                <p><i class="fa fa-check text-primary me-3"></i>Professionalisme</p>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab-pane-3">
                        <div class="row g-4">
                            <div class="col-md-6" style="min-height: 350px;">
                                <div class="position-relative h-100">
                                    <img class="position-absolute img-fluid w-100 h-100" src="{{asset('front/img/projects/villa-sonou.jpg') }}"
                                        style="object-fit: cover;" alt="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h1 class="mb-3">10 Années d'expérience dans l'architecture et l'industrie du design intérieur</h1>
                                <p class="mb-4">Ces projets ont été réalisés par nos experts avec le plus grand soin. Et nous vous les présentons.</p>
                                <p><i class="fa fa-check text-primary me-3"></i>Satisfaction du client</p>
                                <p><i class="fa fa-check text-primary me-3"></i>Délais respectés</p>
                                <p><i class="fa fa-check text-primary me-3"></i>Professionalisme</p>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab-pane-4">
                        <div class="row g-4">
                            <div class="col-md-6" style="min-height: 350px;">
                                <div class="position-relative h-100">
                                    <img class="position-absolute img-fluid w-100 h-100" src="{{asset('front/img/projects/villa-serge.jpg') }}"
                                        style="object-fit: cover;" alt="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h1 class="mb-3">10 Années d'expérience dans l'architecture et l'industrie du design intérieur</h1>
                                <p class="mb-4">Ces projets ont été réalisés par nos experts avec le plus grand soin. Et nous vous les présentons.</p>
                                <p><i class="fa fa-check text-primary me-3"></i>Satisfaction du client</p>
                                <p><i class="fa fa-check text-primary me-3"></i>Délais respectés</p>
                                <p><i class="fa fa-check text-primary me-3"></i>Professionalisme</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Project End -->

    
@endsection