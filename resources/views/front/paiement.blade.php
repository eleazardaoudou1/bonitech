@extends('layouts.front.layout1')
@section('content')
    <div class="container-fluid page-header py-5 mb-5 wow fadeIn" data-wow-delay="0.1s"
        style="visibility: visible; animation-delay: 0.1s; animation-name: fadeIn;">
        <div class="container py-5">
            <h1 class="display-1 text-white  slideInDown">Store</h1>
            <nav aria-label="breadcrumb animated slideInDown">
                <ol class="breadcrumb text-uppercase mb-0">
                    <li class="breadcrumb-item"><a class="text-white" href="{{ route('home') }}">Accueil</a></li>
                    <li class="breadcrumb-item"><a class="text-white" href="#">Pages</a></li>
                    <li class="breadcrumb-item text-white" aria-current="page">Store</li>
                    <li class="breadcrumb-item text-primary active" aria-current="page">Paiement</li>
                </ol>
            </nav>
        </div>
    </div>
    <section class="checkout_area section_gap">
        <div class="container">
           {{--  <div class="returning_customer">
                <div class="check_title">
                    <h2>Vous êtes un de nos clients et vous avez déjà un compte ?
                        <!--a href="#">Click here to login</a></h2-->
                </div>
                <p>Entrez vos informations de connexion puis connectez-vous </p>
                <form class="row contact_form" action="#" method="post" novalidate="novalidate">
                    <div class="col-md-6 form-group p_star">
                        <input type="text" class="form-control" id="name" name="name">
                        <span class="placeholder" data-placeholder="Username or Email"></span>
                    </div>
                    <div class="col-md-6 form-group p_star">
                        <input type="password" class="form-control" id="password" name="password">
                        <span class="placeholder" data-placeholder="Password"></span>
                    </div>
                    <div class="col-md-12 form-group">
                        <button type="submit" value="submit" class="primary-btn">Se connecter</button>
                        <div class="creat_account">
                            <input type="checkbox" id="f-option" name="selector">
                            <label for="f-option">Se souvenir de moi</label>
                        </div>
                        <a class="lost_pass" href="#">Mot de passe oublié ?</a>
                    </div>
                </form>
            </div> --}}
            <div class="cupon_area">
                <div class="check_title">
                    <h2>Vous avez un coupon ? Entrez le code coupon dans la case ci dessous.</h2>
                </div>
                <input type="text" placeholder="Entrer code coupon">
                <a class="tp_btn" href="#">Appliquer Coupon</a>
            </div>
            <div class="billing_details">
                <div class="row">
                    <div class="col-lg-8">
                        <h3>Informations de paiement</h3>
                        <form class="row contact_form" action="#" method="post" id="paiementForm"
                            novalidate="novalidate">
                            @csrf
                            <div class="col-md-6 form-group p_star">
                                <input type="text" class="form-control" id="nom" name="nom" required>
                                <span class="placeholder" data-placeholder="Nom"></span>
                            </div>
                            <div class="col-md-6 form-group p_star">
                                <input type="text" class="form-control" id="prenom" name="prenom" required>
                                <span class="placeholder" data-placeholder="Prenom"></span>
                            </div>
                            <div class="col-md-12 form-group">
                                <input type="text" class="form-control" id="company" name="company"
                                    placeholder="Nom de votre entreprise">
                            </div>
                            <div class="col-md-6 form-group p_star">
                                <input type="text" class="form-control" id="numero" name="numero" required>
                                <span class="placeholder" data-placeholder="Telephone"></span>
                            </div>
                          {{--   <div class="col-md-6 form-group p_star">
                                <input type="text" class="form-control" id="email" name="email" required>
                                <span class="placeholder" data-placeholder="Email"></span>
                            </div> --}}
                            {{--  <div class="col-md-12 form-group p_star">
                                <select class="country_select" style="display: none;">
                                    <option value="1">Country</option>
                                    <option value="2">Country</option>
                                    <option value="4">Country</option>
                                </select>
                                <div class="nice-select country_select" tabindex="0"><span
                                        class="current">Country</span>
                                    <ul class="list">
                                        <li data-value="1" class="option selected">Country</li>
                                        <li data-value="2" class="option">Country</li>
                                        <li data-value="4" class="option">Country</li>
                                    </ul>
                                </div>
                            </div> --}}
                            <div class="col-md-12 form-group p_star">
                                <input type="text" class="form-control" id="adresse1" name="adresse1" required>
                                <span class="placeholder" data-placeholder="Détails de l'adresse 01"></span>
                            </div>
                            <div class="col-md-12 form-group p_star">
                                <input type="text" class="form-control" id="adresse2" name="adresse2" >
                                <span class="placeholder" data-placeholder="Détails de l'adresse 02"></span>
                            </div>
                            <div class="col-md-12 form-group p_star">
                                <input type="text" class="form-control" id="ville" name="ville" required>
                                <span class="placeholder" data-placeholder="Ville"></span>
                            </div>
                            {{-- <div class="col-md-12 form-group p_star">
                                <select class="country_select" style="display: none;">
                                    <option value="1">District</option>
                                    <option value="2">District</option>
                                    <option value="4">District</option>
                                </select>
                                <div class="nice-select country_select" tabindex="0"><span
                                        class="current">District</span>
                                    <ul class="list">
                                        <li data-value="1" class="option selected">District</li>
                                        <li data-value="2" class="option">District</li>
                                        <li data-value="4" class="option">District</li>
                                    </ul>
                                </div>
                            </div> --}}
                            <div class="col-md-12 form-group">
                                <input type="text" class="form-control" id="zip" name="zip"
                                    placeholder="Postcode/ZIP">
                            </div>
                           {{--  <div class="col-md-12 form-group">
                                <div class="creat_account">
                                    <input type="checkbox" id="f-option2" name="selector">
                                    <label for="f-option2">Créer un compte ?</label>
                                </div>
                            </div> --}}
                            <div class="col-md-12 form-group">
                                {{-- <div class="creat_account">
                                    <h3>Shipping Details</h3>
                                    <input type="checkbox" id="f-option3" name="selector">
                                    <label for="f-option3">Ship to a different address?</label>
                                </div> --}}
                                <textarea class="form-control" required name="message" id="message" rows="1"
                                    placeholder="Notes importantes sur la commande"></textarea>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-4">
                        <div class="order_box">
                            <h2>Votre commande</h2>
                            <ul class="list">
                                <li><a href="#">Produit <span>Total</span></a></li>
                                @foreach (session('commande')->getItems() as $hash => $item)
                                    <li><a href="#">{{ $item->getDetails()->title }} <span class="middle"> x
                                                {{ $item->getDetails()->quantity }}</span> <span
                                                class="last">{{ $item->getDetails()->price }} </span></a></li>
                                @endforeach
                            </ul>
                            <ul class="list list_2">
                                <li><a href="#">Sous-Total <span>{{ session('commande')->getSubTotal() }}
                                            CFA</span></a></li>
                                <li><a href="#">Frais de livraison <span>00.00 CFA</span></a></li>
                                <li><a href="#">Total <span>{{ session('commande')->getTotal() }} CFA</span></a></li>
                            </ul>
                            <div class="payment_item">
                                <div class="radion_btn">
                                    <input type="radio" id="f-option5" value="cheque" name="selector">
                                    <label for="f-option5">Paiement par chèque</label>
                                    <div class="check"></div>
                                </div>
                                <p>Sélectionnez un moyen de paiement </p>
                            </div>

                            <div class="payment_item active mb-5">
                               {{--  <div class="radion_btn">
                                    <input type="radio" id="f-option6" value="paypal" name="selector"
                                        style="height: 80px, width: 50px">
                                    <label for="f-option6">Paypal </label>
                                    <img src="{{ asset('front/img/product/card.jpg') }}" alt="">
                                    <div class="check"></div>
                                </div> --}}
                                <div class="row" id="paypalBtn">
                                    <div class="col-md-12" id="paypalBtn_1"
                                        style=" background: no-repeat center/60% url('{{ asset('front/img/icons/paypal.png') }}')">
                                    </div>
                                </div>
                                {{--  <p ></p> --}}

                            </div>

                            <div class="payment_item active">
                               {{--  <div class="radion_btn">
                                    <input type="radio" id="f-option7" value="fedapay" name="selector">
                                    <label for="f-option7">FEDAPAY </label>
                                    <img src="{{ asset('front/img/product/card.jpg') }}" alt="">
                                    <div class="check"></div>
                                </div> --}}
                                <div class="row" id="fedapayBtn">
                                    <div class="col-md-12" id="fedapayBtn_1"
                                        style=" background: no-repeat center/30% url('{{ asset('front/img/icons/fedapay.png') }}')">
                                    </div>
                                </div>
                                {{-- <p>Payer via KKIPAY; </p> --}}
                            </div>


                            <div class="creat_account">
                                <input type="checkbox" id="termsCheck" name="selector">
                                <label for="f-option4">J'ai lu et j'approuve les </label>
                                <a href="#">termes &amp; conditions*</a>
                            </div>
                            <a class="primary-btn" disabled id="btn_payer">Payer</a>
                            <p class="erreur_choix" id="output"></p>
                            {{-- <button id="pay-btn" data-transaction-amount="1000"
                            data-transaction-description="Acheter mon produit" data-customer-email="johndoe@gmail.com"
                            data-customer-lastname="Doe">Payer 1000 FCFA</button>  --}}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        let btnFeday = document.getElementById('fedapayBtn_1');
        let btnPaypal = document.getElementById('paypalBtn_1');
        let termsCheck = document.getElementById('termsCheck');

        btnFeday.addEventListener("click", function handleClick(event) {
            //this.className = "selectedPaypal"
            event.target.classList.add("selectedPayBtn");
            btnPaypal.classList.remove("selectedPayBtn");
        });


        btnPaypal.addEventListener("click", function handleClick(event) {
            //this.className = "selectedPaypal"
            event.target.classList.add("selectedPayBtn"); //.add('selectedPayBtn');
            btnFeday.classList.remove("selectedPayBtn");
        });


        let btn = document.getElementById('btn_payer');
        let widget = FedaPay.init({
            public_key: 'pk_live_Eg4g51UCVQzljtOsi_IJDwvr',
            transaction: {
                amount: {{ session('commande')->getTotal() }},
                description: 'Acheter le produit'
            },
            customer: {
                email: 'johndoe@gmail.com',
                lastname: 'Doe',
                firstname: 'John',
            }
        });

        btn.addEventListener("click", () => {

           
            //recuperer le formuaire
            let paiementForm = document.getElementById("paiementForm");

            //soumettre le formulaire
            paiementForm.addEventListener("submit", (e) => {
                e.preventDefault();

                let nom = document.getElementById("nom");
                let prenom = document.getElementById("prenom");

                if (nom.value == "" || prenom.value == "") {
                    console.log();
                } else {
                    console.log(nom.value);
                    // perform operation with form input
                    alert("This form has been successfully submitted!");
                    console.log(
                        `This form has a username of ${username.value} and password of ${password.value}`
                    );

                    username.value = "";
                    password.value = "";
                }
            });

            if(! btnFeday.classList.contains("selectedPayBtn") && !  btnPaypal.classList.contains("selectedPayBtn")){
                output.innerText = `Choisissez un moyen de paiement`;
                return;
            }

            else if(termsCheck.checked==false) {
                output.innerText = `Cochez la case des termes et conditions`;
                return;
            }
            else{
                output.innerText = ``;
            }


            if (btnFeday.classList.contains("selectedPayBtn")) {
                widget.open();
            }
        });
    </script>
@endsection
